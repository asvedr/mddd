pub trait IUseCaseRequest: Sized {
    fn build(src: &[&str]) -> Result<Self, String>;
    fn help() -> String;
}

pub trait IConfig: Sized {
    fn build() -> Result<Self, String>;
    fn help() -> String;
}

pub trait IUseCase {
    type Request;
    type Error;
    fn short_description() -> String;
    fn description(&self) -> String {
        Self::short_description()
    }
    fn execute(&mut self, request: Self::Request) -> Result<(), Self::Error>;
}

pub trait IParser<T> {
    fn parse(src: &str) -> Result<T, String>;
}

pub trait IRunner {
    fn short_description(&self) -> String;
    fn execute(&mut self, prefix: &str, args: &[&str]);
}

impl IUseCaseRequest for () {
    fn build(src: &[&str]) -> Result<Self, String> {
        if src.is_empty() {
            Ok(())
        } else {
            Err("No args expected".to_string())
        }
    }

    fn help() -> String {
        "<Nothing>".to_string()
    }
}

pub trait CacheParam {
    type CacheType: Eq;
    fn to_cache_type(&self) -> Self::CacheType;
}

macro_rules! impl_clone {
    ($tp:ty) => {
        impl CacheParam for $tp {
            type CacheType = $tp;
            fn to_cache_type(&self) -> $tp {
                self.clone()
            }
        }
    };
}

impl_clone!(usize);
impl_clone!(isize);
impl_clone!(u64);
impl_clone!(i64);
impl_clone!(u32);
impl_clone!(i32);
impl_clone!(u16);
impl_clone!(i16);
impl_clone!(u8);
impl_clone!(i8);
impl_clone!(char);
impl_clone!(String);

impl<T: CacheParam> CacheParam for &T {
    type CacheType = T::CacheType;

    fn to_cache_type(&self) -> T::CacheType {
        (**self).to_cache_type()
    }
}

impl CacheParam for &str {
    type CacheType = String;

    fn to_cache_type(&self) -> Self::CacheType {
        self.to_string()
    }
}

impl<T: CacheParam> CacheParam for &[T] {
    type CacheType = Vec<T::CacheType>;

    fn to_cache_type(&self) -> Self::CacheType {
        self.iter().map(|i| i.to_cache_type()).collect()
    }
}

impl<T: CacheParam> CacheParam for Vec<T> {
    type CacheType = Vec<T::CacheType>;

    fn to_cache_type(&self) -> Self::CacheType {
        self.iter().map(|i| i.to_cache_type()).collect()
    }
}

impl<A: CacheParam, B: CacheParam> CacheParam for (A, B) {
    type CacheType = (A::CacheType, B::CacheType);

    fn to_cache_type(&self) -> Self::CacheType {
        (self.0.to_cache_type(), self.1.to_cache_type())
    }
}

impl<A: CacheParam, B: CacheParam, C: CacheParam> CacheParam for (A, B, C) {
    type CacheType = (A::CacheType, B::CacheType, C::CacheType);

    fn to_cache_type(&self) -> Self::CacheType {
        (
            self.0.to_cache_type(),
            self.1.to_cache_type(),
            self.2.to_cache_type(),
        )
    }
}

impl<A: CacheParam, B: CacheParam, C: CacheParam, D: CacheParam> CacheParam
for (A, B, C, D)
{
    type CacheType = (A::CacheType, B::CacheType, C::CacheType, D::CacheType);

    fn to_cache_type(&self) -> Self::CacheType {
        (
            self.0.to_cache_type(),
            self.1.to_cache_type(),
            self.2.to_cache_type(),
            self.3.to_cache_type(),
        )
    }
}

impl<
    A: CacheParam,
    B: CacheParam,
    C: CacheParam,
    D: CacheParam,
    E: CacheParam,
> CacheParam for (A, B, C, D, E)
{
    type CacheType = (
        A::CacheType,
        B::CacheType,
        C::CacheType,
        D::CacheType,
        E::CacheType,
    );

    fn to_cache_type(&self) -> Self::CacheType {
        (
            self.0.to_cache_type(),
            self.1.to_cache_type(),
            self.2.to_cache_type(),
            self.3.to_cache_type(),
            self.4.to_cache_type(),
        )
    }
}
