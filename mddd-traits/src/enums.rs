#[derive(Debug, PartialEq, Eq)]
pub struct InvalidEnumValue<Val> {
    pub typename: &'static str,
    pub value: Val,
}

pub trait IStrEnum: Sized {
    fn variants() -> &'static [Self];
    fn val_to_str(&self) -> &'static str;
    fn str_to_val(src: &str) -> Result<Self, InvalidEnumValue<String>>;
}

pub trait IIntEnum: Sized {
    fn variants() -> &'static [Self];
    fn val_to_int(&self) -> isize;
    fn int_to_val(src: isize) -> Result<Self, InvalidEnumValue<isize>>;
}
