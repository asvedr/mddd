use mddd::macros::EnumAutoFrom;

#[derive(EnumAutoFrom, Debug)]
enum NoGenerics {
    Int(usize),
    #[accept(f64)]
    #[accept(f32)]
    Real(f64),
    Str(&'static str),
}

#[derive(EnumAutoFrom, Debug)]
enum Generics<'a> {
    Msg(&'a str),
    #[accept(String)]
    Other,
}

#[derive(EnumAutoFrom, Debug)]
enum Custom {
    #[accept(Vec<usize>)]
    #[accept(usize)]
    #[method(|x| format!("{:?}", x) )]
    Value(String),
}


fn main() {
    let no_gen: &[NoGenerics] = &[1.into(), 2.0_f32.into(), 3.0_f64.into(), "str".into()];
    println!("{:?}", no_gen);
    let gen: &[Generics<'static>] = &["x".into(), "x".to_string().into()];
    println!("{:?}", gen);
    let custom: &[Custom] = &[5_usize.into(), vec![1_usize,2,3].into()];
    println!("{:?}", custom);
}

