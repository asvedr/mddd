use std::env;

use mddd::macros::lru_cache;

#[lru_cache]
fn singleton() -> Vec<usize> {
    println!("called");
    vec![1, 2, 3]
}

#[lru_cache(cache_len = 2)]
fn cached_add(a: usize, b: usize) -> usize {
    println!("called: {} + {}", a, b);
    a + b
}

#[lru_cache(ttl = 1)]
fn ttl_now() -> usize {
    println!("called");
    1
}

#[lru_cache(ttl = 1)]
fn ttl_add(a: usize, b: usize) -> usize {
    println!("called");
    a + b
}

#[lru_cache(no_mutex)]
fn singleton_nmtx() -> Vec<usize> {
    println!("called");
    vec![1, 2, 3]
}

#[lru_cache(cache_len = 2, no_mutex)]
fn cached_add_nmtx(a: usize, b: usize) -> usize {
    println!("called: {} + {}", a, b);
    a + b
}

#[lru_cache(ttl = 1, no_mutex)]
fn ttl_now_nmtx() -> usize {
    println!("called");
    1
}

#[lru_cache(ttl = 1, no_mutex)]
fn ttl_add_nmtx(a: usize, b: usize) -> usize {
    println!("called");
    a + b
}

fn case_singleton() {
    println!("> {:?}", singleton());
    println!("> {:?}", singleton());
    println!("> no_mutex <");
    println!("> {:?}", singleton_nmtx());
    println!("> {:?}", singleton_nmtx());
}

fn case_parametrized() {
    println!("> {:?}", cached_add(1, 2));
    println!("> {:?}", cached_add(1, 2));

    println!("> {:?}", cached_add(3, 5));
    println!("> {:?}", cached_add(3, 5));
    println!("> {:?}", cached_add(1, 2));

    println!("> {:?}", cached_add(5, 5));
    println!("> {:?}", cached_add(5, 5));
    println!("> {:?}", cached_add(1, 2));
    println!("> no_mutex <");
    println!("> {:?}", cached_add_nmtx(1, 2));
    println!("> {:?}", cached_add_nmtx(1, 2));

    println!("> {:?}", cached_add_nmtx(3, 5));
    println!("> {:?}", cached_add_nmtx(3, 5));
    println!("> {:?}", cached_add_nmtx(1, 2));

    println!("> {:?}", cached_add_nmtx(5, 5));
    println!("> {:?}", cached_add_nmtx(5, 5));
    println!("> {:?}", cached_add_nmtx(1, 2));
}

fn case_ttl_no_params() {
    use std::thread;
    use std::time::Duration;

    println!("> {}", ttl_now());
    println!("> {}", ttl_now());
    thread::sleep(Duration::from_secs(2));
    println!("> {}", ttl_now());
    println!("> no_mutex <");
    println!("> {}", ttl_now_nmtx());
    println!("> {}", ttl_now_nmtx());
    thread::sleep(Duration::from_secs(2));
    println!("> {}", ttl_now_nmtx());
}

fn case_ttl_params() {
    use std::thread;
    use std::time::Duration;

    println!("> {}", ttl_add(1, 2));
    println!("> {}", ttl_add(1, 2));
    thread::sleep(Duration::from_secs(2));
    println!("> {}", ttl_add(1, 2));
    println!("> no_mutex <");
    println!("> {}", ttl_add_nmtx(1, 2));
    println!("> {}", ttl_add_nmtx(1, 2));
    thread::sleep(Duration::from_secs(2));
    println!("> {}", ttl_add_nmtx(1, 2));
}

fn main() {
    let args = env::args().collect::<Vec<_>>();
    match &*args[1] {
        "case_singleton" => case_singleton(),
        "case_parametrized" => case_parametrized(),
        "case_ttl_no_params" => case_ttl_no_params(),
        "case_ttl_params" => case_ttl_params(),
        _ => panic!("unknown arg: {}", args[1]),
    }
}
