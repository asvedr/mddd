mod str_enum;
mod int_enum;

use std::thread;

use mddd::macros::singleton;

static mut CALLED: usize = 0;
static mut FORCE_TY_1: usize = 0;
static mut FORCE_TY_2: usize = 0;

#[singleton(no_mutex)]
fn single_vec() -> Vec<usize> {
    unsafe { CALLED += 1 };
    vec![1, 2, 3]
}

#[singleton]
fn single_m_vec() -> Vec<usize> {
    thread::yield_now();
    unsafe { CALLED += 1 };
    thread::yield_now();
    vec![1, 2, 3]
}

#[singleton(no_mutex)]
fn parametrized_simple_1(arg_key: usize) -> usize {
    unsafe {CALLED += 1};
    arg_key + 2
}

#[singleton]
fn parametrized_simple_2(key1: usize, key2: usize) -> String {
    unsafe {CALLED += 1};
    (key1 + key2).to_string()
}

#[singleton(mutex)]
fn parametrized_mutex_1(arg_key: usize) -> usize {
    unsafe {CALLED += 1};
    arg_key + 2
}


#[singleton]
fn parametrized_mutex_2(key1: usize, key2: usize) -> String {
    unsafe {CALLED += 1};
    (key1 + key2).to_string()
}

#[singleton(no_mutex, ty=&'static dyn ToString)]
fn force_ty1() -> usize {
    unsafe {FORCE_TY_1 += 1};
    1
}

#[singleton(ty=&'static dyn ToString)]
fn force_ty2() -> String {
    unsafe {FORCE_TY_2 += 1};
    "hello".to_string()
}

fn case_simple() {
    assert_eq!(single_vec(), &[1, 2, 3]);
    assert_eq!(single_vec(), &[1, 2, 3]);
    println!("{}", unsafe { CALLED });
}

fn case_simple_param_1() {
    println!("CALL SINGLE(1): {}", parametrized_simple_1(1));
    println!("CALL SINGLE(1): {}", parametrized_simple_1(1));
    println!("CALL SINGLE(2): {}", parametrized_simple_1(2));
    println!("CALL SINGLE(2): {}", parametrized_simple_1(2));
    println!("{}", unsafe {CALLED});
}

fn case_simple_param_2() {
    println!("CALL SINGLE(1,1): {}", parametrized_simple_2(1,1));
    println!("CALL SINGLE(1,1): {}", parametrized_simple_2(1,1));
    println!("CALL SINGLE(1,2): {}", parametrized_simple_2(1,2));
    println!("CALL SINGLE(1,2): {}", parametrized_simple_2(1,2));
    println!("{}", unsafe {CALLED});
}


fn case_mutex_param_1() {
    println!("CALL SINGLE(1): {}", parametrized_mutex_1(1));
    println!("CALL SINGLE(1): {}", parametrized_mutex_1(1));
    println!("CALL SINGLE(2): {}", parametrized_mutex_1(2));
    println!("CALL SINGLE(2): {}", parametrized_mutex_1(2));
    println!("{}", unsafe {CALLED});
}

fn case_mutex_param_2() {
    println!("CALL SINGLE(1,1): {}", parametrized_mutex_2(1,1));
    println!("CALL SINGLE(1,1): {}", parametrized_mutex_2(1,1));
    println!("CALL SINGLE(1,2): {}", parametrized_mutex_2(1,2));
    println!("CALL SINGLE(1,2): {}", parametrized_mutex_2(1,2));
    println!("{}", unsafe {CALLED});
}

fn case_mutex() {
    let mut handlers = Vec::new();
    for _ in 0..10 {
        handlers.push(thread::spawn(|| single_m_vec()));
    }
    for handler in handlers {
        let val = handler.join().unwrap();
        assert_eq!(val, &[1, 2, 3]);
    }
    println!("{}", unsafe { CALLED });
}

fn case_force_ty() {
    let vals = &[force_ty1(), force_ty2()];
    force_ty1();
    force_ty2();
    println!("{} {}", vals[0].to_string(), vals[1].to_string());
    unsafe {
        println!("CNT: {} {}", FORCE_TY_1, FORCE_TY_2);
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    match &args[1][..] {
        "case_simple" => case_simple(),
        "case_mutex" => case_mutex(),
        "case_simple_param_1" => case_simple_param_1(),
        "case_simple_param_2" => case_simple_param_2(),
        "case_mutex_param_1" => case_mutex_param_1(),
        "case_mutex_param_2" => case_mutex_param_2(),
        "case_force_ty" => case_force_ty(),
        _ => panic!("unexpected case"),
    }
}
