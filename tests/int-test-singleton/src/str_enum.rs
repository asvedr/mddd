use mddd::macros::IStrEnum;

#[derive(IStrEnum, Debug, Eq, PartialEq)]
pub enum SEnum {
    VarA,
    #[str_value("var_b")]
    VarB,
    VarC
}

#[cfg(test)]
mod tests {
    use super::*;
    use mddd::traits::{IStrEnum, InvalidEnumValue};

    #[test]
    fn test_val_to_str() {
        assert_eq!(SEnum::VarA.val_to_str(), "VarA");
    }

    #[test]
    fn test_str_to_val_ok() {
        assert_eq!(SEnum::str_to_val("VarA"), Ok(SEnum::VarA));
    }

    #[test]
    fn test_s_val() {
        assert_eq!(SEnum::VarB.val_to_str(), "var_b");
        assert_eq!(SEnum::str_to_val("var_b"), Ok(SEnum::VarB));
    }

    #[test]
    fn test_str_to_val_err() {
        let expected = InvalidEnumValue {
            typename: "SEnum",
            value: "abc".to_string()
        };
        assert_eq!(SEnum::str_to_val("abc"), Err(expected))
    }

    #[test]
    fn test_all_variants() {
        assert_eq!(
            SEnum::variants(),
            &[
                SEnum::VarA,
                SEnum::VarB,
                SEnum::VarC
            ]
        )
    }
}
