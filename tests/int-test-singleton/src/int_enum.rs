use mddd::macros::IIntEnum;

#[derive(IIntEnum, Debug, Eq, PartialEq)]
pub enum IEnum {
    VarA,
    #[int_value(-12)]
    VarB,
    #[int_value(10)]
    VarC
}

#[cfg(test)]
mod tests {
    use super::*;
    use mddd::traits::{IIntEnum, InvalidEnumValue};

    #[test]
    fn test_val_to_int() {
        assert_eq!(IEnum::VarA.val_to_int(), 0);
    }

    #[test]
    fn test_int_to_val_ok() {
        assert_eq!(IEnum::int_to_val(0), Ok(IEnum::VarA));
    }

    #[test]
    fn test_s_val() {
        assert_eq!(IEnum::VarB.val_to_int(), -12);
        assert_eq!(IEnum::int_to_val(-12), Ok(IEnum::VarB));
    }

    #[test]
    fn test_int_to_val_err() {
        let expected = InvalidEnumValue {
            typename: "IEnum",
            value: 111
        };
        assert_eq!(IEnum::int_to_val(111), Err(expected))
    }

    #[test]
    fn test_all_variants() {
        assert_eq!(
            IEnum::variants(),
            &[
                IEnum::VarA,
                IEnum::VarB,
                IEnum::VarC
            ]
        )
    }
}
