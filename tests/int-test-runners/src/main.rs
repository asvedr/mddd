use mddd::std::{LeafRunner, NodeRunner};

mod use_cases;

use use_cases as uc;

fn main() {
    NodeRunner::new("runner int test")
        .add("hello", LeafRunner::new(|| uc::hello::HelloUC))
        .add(
            "math",
            NodeRunner::new("math use cases")
                .add("+", LeafRunner::new(|| uc::add::AddUC))
                .add("-", LeafRunner::new(|| uc::sub::SubUC)),
        )
        .set_default_params(&["math", "+", "1", "2"])
        .run()
}
