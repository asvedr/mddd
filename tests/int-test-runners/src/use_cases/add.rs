use mddd::macros::IUseCaseRequest;
use mddd::traits::{IUseCase, IUseCaseRequest};

pub struct AddUC;

#[derive(IUseCaseRequest)]
pub struct Request {
    #[description("list of operands")]
    #[multi]
    args: Vec<isize>,
}

impl IUseCase for AddUC {
    type Request = Request;
    type Error = ();

    fn short_description() -> String {
        "sum of several int numbers".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        let res: isize = request.args.iter().cloned().sum();
        println!("{}", res);
        Ok(())
    }
}
