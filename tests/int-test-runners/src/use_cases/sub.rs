use mddd::macros::IUseCaseRequest;
use mddd::traits::{IUseCase, IUseCaseRequest};

pub struct SubUC;

#[derive(IUseCaseRequest)]
pub struct Request {
    a: isize,
    b: isize,
}

impl IUseCase for SubUC {
    type Request = Request;
    type Error = ();

    fn short_description() -> String {
        "a - b".to_string()
    }

    fn execute(&mut self, request: Self::Request) -> Result<(), Self::Error> {
        println!("{}", request.a - request.b);
        Ok(())
    }
}
