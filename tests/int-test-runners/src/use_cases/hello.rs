use mddd::traits::IUseCase;

pub struct HelloUC;

impl IUseCase for HelloUC {
    type Request = ();
    type Error = ();

    fn short_description() -> String {
        "say hello world".to_string()
    }

    fn execute(&mut self, _: Self::Request) -> Result<(), Self::Error> {
        println!("Hello world!");
        Ok(())
    }
}
