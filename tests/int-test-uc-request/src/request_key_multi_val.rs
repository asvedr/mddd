use std::collections::HashSet;

use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCaseRequest;

#[derive(IUseCaseRequest, Debug, Eq, PartialEq)]
struct Request {
    #[multi]
    #[short("-v")]
    #[description("vec of int")]
    v_val: Vec<usize>,
    #[multi]
    #[short("-s")]
    #[description("set of int")]
    s_val: HashSet<usize>,
    #[short("-o")]
    #[default(1)]
    other: usize,
}

#[test]
fn test_default_ok() {
    let val = Request::build(&[]).unwrap();
    let expected = Request {
        v_val: vec![],
        s_val: Default::default(),
        other: 1,
    };
    assert_eq!(val, expected)
}

#[test]
fn test_values_ok() {
    let val = Request::build(&["-v", "10", "-s", "2", "-o", "30", "-v", "11", "-s", "3"]).unwrap();
    let expected = Request {
        v_val: vec![10, 11],
        s_val: [2, 3].into_iter().collect(),
        other: 30,
    };
    assert_eq!(val, expected)
}

#[test]
fn test_parse_error() {
    let err = Request::build(&["-v", "10", "-v", "a"]).unwrap_err();
    assert_eq!(err, "Invalid field(v_val): expected type usize");
}

#[test]
fn test_key_has_no_value() {
    let err = Request::build(&["-v", "10", "-v"]).unwrap_err();
    assert_eq!(err, "Invalid field(v_val): KeyRequiresValue");
}

#[test]
fn test_help() {
    let expected = concat!(
        "  -v many[v_val]: usize, vec of int\n",
        "  -s many[s_val]: usize, set of int\n",
        "  -o other: usize, (default=1)\n"
    );
    assert_eq!(expected, Request::help())
}
