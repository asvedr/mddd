use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCaseRequest;

#[derive(IUseCaseRequest, Debug, Eq, PartialEq)]
struct Request {
    #[bool_flag]
    #[short("-f")]
    #[description("hehe")]
    flag: bool,
    pos: usize,
    #[short("-k")]
    key: usize,
}

#[test]
fn test_ok() {
    let request = Request::build(&["-f", "1", "-k", "2"]).unwrap();
    assert_eq!(
        request,
        Request {
            flag: true,
            pos: 1,
            key: 2
        }
    );
    let request = Request::build(&["1", "-k", "2", "-f"]).unwrap();
    assert_eq!(
        request,
        Request {
            flag: true,
            pos: 1,
            key: 2
        }
    );
    let request = Request::build(&["-k", "2", "1"]).unwrap();
    assert_eq!(
        request,
        Request {
            flag: false,
            pos: 1,
            key: 2
        }
    )
}

#[test]
fn test_help() {
    let expected = concat!(
        "  -f flag: bool flag, hehe\n",
        "  pos: usize\n",
        "  -k key: usize\n"
    );
    assert_eq!(Request::help(), expected);
}
