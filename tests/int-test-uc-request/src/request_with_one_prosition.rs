use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCaseRequest;

#[derive(IUseCaseRequest, Debug, Eq, PartialEq)]
struct Request {
    #[description("common keyed param")]
    #[short("-k")]
    key_param: i8,
    #[description("common position param")]
    pos_param: i8,
}

#[test]
fn test_ok() {
    let expected = Request {
        key_param: 1,
        pos_param: 2,
    };
    let request = Request::build(&["-k", "1", "2"]).unwrap();
    assert_eq!(expected, request);
    let request = Request::build(&["2", "-k", "1"]).unwrap();
    assert_eq!(expected, request);
}

#[test]
fn test_no_position_value() {
    let err = Request::build(&["-k", "1"]).unwrap_err();
    assert_eq!(err, "Field not set: pos_param");
}

#[test]
fn test_help() {
    let expected = concat!(
        "  -k key_param: i8, common keyed param\n",
        "  pos_param: i8, common position param\n"
    );
    assert_eq!(expected, Request::help())
}
