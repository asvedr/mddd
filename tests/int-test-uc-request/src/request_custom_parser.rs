use mddd::macros::IUseCaseRequest;
use mddd::traits::{IParser, IUseCaseRequest};

#[derive(Eq, PartialEq, Debug)]
enum Enum {
    A,
    B,
}

struct EnumParser;

impl IParser<Enum> for EnumParser {
    fn parse(src: &str) -> Result<Enum, String> {
        match src {
            "A" | "a" | "1" => Ok(Enum::A),
            "B" | "b" | "2" => Ok(Enum::B),
            _ => Err(format!("invalid enum value: {:?}", src)),
        }
    }
}

#[derive(IUseCaseRequest, Debug, Eq, PartialEq)]
struct Request {
    #[parser(EnumParser)]
    single: Enum,
    #[multi]
    #[parser(EnumParser)]
    multi: Vec<Enum>,
}

#[test]
fn test_ok() {
    let request = Request::build(&["a", "2", "A", "1", "b"]).unwrap();
    let expected = Request {
        single: Enum::A,
        multi: vec![Enum::B, Enum::A, Enum::A, Enum::B],
    };
    assert_eq!(request, expected);
}

#[test]
fn test_help() {
    let expected = concat!("  single: Enum\n", "  many[multi]: Enum\n");
    assert_eq!(Request::help(), expected);
}
