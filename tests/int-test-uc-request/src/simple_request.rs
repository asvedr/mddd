use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCaseRequest;

#[derive(IUseCaseRequest, Debug, Eq, PartialEq)]
struct Request {
    #[description("simple int param")]
    #[default(1)]
    #[short("-i")]
    #[long("--int")]
    int_param: usize,
    #[short("-s")]
    str_param: String,
}

#[test]
fn test_ok() {
    let expected = Request {
        int_param: 10,
        str_param: "abc".to_string(),
    };
    let request = Request::build(&["-i", "10", "-s", "abc"]).unwrap();
    assert_eq!(request, expected);
    let request = Request::build(&["-s", "abc", "--int", "10"]).unwrap();
    assert_eq!(request, expected);
    let expected = Request {
        int_param: 1,
        str_param: "xyz".to_string(),
    };
    let request = Request::build(&["-s", "xyz"]).unwrap();
    assert_eq!(request, expected);
}

#[test]
fn test_key_not_set() {
    let err = Request::build(&[]).unwrap_err();
    assert_eq!(err, "Field not set: str_param")
}

#[test]
fn test_key_has_no_value() {
    let err = Request::build(&["-s", "x", "-i"]).unwrap_err();
    assert_eq!(err, "Invalid field(int_param): KeyRequiresValue");
}

#[test]
fn test_parse_error() {
    let err = Request::build(&["-s", "a", "-i", "b"]).unwrap_err();
    assert_eq!(err, "Invalid field(int_param): expected type usize");
}

#[test]
fn test_help() {
    let expected = concat!(
        "  -i --int int_param: usize, (default=1), simple int param\n",
        "  -s str_param: String\n"
    );
    assert_eq!(expected, Request::help())
}
