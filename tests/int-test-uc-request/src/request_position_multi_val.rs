use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCaseRequest;

#[derive(IUseCaseRequest, Debug, Eq, PartialEq)]
struct RequestNoKeys {
    pos_s1: usize,
    pos_s2: usize,
    #[multi]
    pos_multi: Vec<usize>,
}

#[derive(IUseCaseRequest, Debug, Eq, PartialEq)]
struct RequestKeys {
    #[multi]
    pos_multi: Vec<usize>,
    #[short("-k")]
    key: usize,
}

#[test]
fn test_ok_no_keys() {
    let request = RequestNoKeys::build(&["4", "3", "2", "1"]).unwrap();
    let expected = RequestNoKeys {
        pos_s1: 4,
        pos_s2: 3,
        pos_multi: vec![2, 1],
    };
    assert_eq!(expected, request);
    let request = RequestNoKeys::build(&["4", "3"]).unwrap();
    let expected = RequestNoKeys {
        pos_s1: 4,
        pos_s2: 3,
        pos_multi: vec![],
    };
    assert_eq!(expected, request);
}

#[test]
fn test_ok_keys() {
    let expected = RequestKeys {
        pos_multi: vec![1, 2],
        key: 3,
    };
    let request = RequestKeys::build(&["1", "-k", "3", "2"]).unwrap();
    assert_eq!(request, expected);
    let request = RequestKeys::build(&["-k", "3", "1", "2"]).unwrap();
    assert_eq!(request, expected);
}
