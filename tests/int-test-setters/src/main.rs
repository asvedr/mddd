use mddd::macros::{StructSetters, singleton};

#[derive(StructSetters, Debug)]
struct S<'a> {
    #[assign]
    pub x: usize,
    #[remake('a)]
    pub y: &'a Vec<f64>,
}

#[singleton(no_mutex)]
fn dflt_vec() -> Vec<f64> {
    vec![1.0]
}

impl<'a> S<'a> {
    pub fn new() -> S<'static> {
        S {x: 0, y: dflt_vec()}
    }
}

fn main() {
    let v = vec![2.0, 3.0];
    let s = S::new()
        .set_x(1)
        .set_y(&v);
    println!("{:?}", s);
}
