#![allow(dead_code)]
use std::env;

use mddd::macros::IConfig;
use mddd::traits::{IConfig, IParser};

struct FlagParser;

impl IParser<usize> for FlagParser {
    fn parse(src: &str) -> Result<usize, String> {
        match src {
            "a" => Ok(1),
            "b" => Ok(3),
            _ => Err(format!("invalid custom value: {:?}", src)),
        }
    }
}

#[derive(IConfig, Debug)]
struct A {
    #[default(3)]
    int_val: usize,
    #[default("hello".to_string())]
    #[description("text name")]
    str_val: String,
    bool_flag: bool,
    #[parser(FlagParser)]
    custom_val: usize,
}

#[derive(IConfig, Debug)]
struct B {
    #[name("i1")]
    #[default(1)]
    int_1_val: usize,
    #[default(2)]
    #[name("i2")]
    int_2_val: usize,
    int_3_val: usize,
    #[prefix("B_")]
    __meta__: (),
}

#[derive(IConfig, Debug)]
struct O {
    a: Option<usize>,
    b: Option<String>,
    #[default(Some(true))]
    c: Option<bool>,
}

fn case_config_help() {
    println!("{}", A::help());
    println!("{}", B::help());
    println!("{}", O::help());
}

fn case_config_a_build() {
    println!("{:?}", A::build());
}

fn case_config_b_build() {
    println!("{:?}", B::build());
}

fn case_config_o_build() {
    println!("{:?}", O::build());
}

fn main() {
    let args = env::args().collect::<Vec<_>>();
    if args.len() < 2 {
        panic!("case arg expected");
    }
    match &args[1][..] {
        "case_config_help" => case_config_help(),
        "case_config_a_build" => case_config_a_build(),
        "case_config_b_build" => case_config_b_build(),
        "case_config_o_build" => case_config_o_build(),
        _ => panic!("unexpected case"),
    }
}

