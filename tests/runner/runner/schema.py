from dataclasses import dataclass
from typing import List, Union, Dict


@dataclass
class Cmd:
    cmd: str
    expected: Union[List[str], str]
    env_vars: Dict[str, str] = None

    @classmethod
    def from_json(cls, src: dict) -> 'Cmd':
        result = cls(**src)
        assert isinstance(result.cmd, str), f"cmd is not str: {result.cmd}"
        if not result.env_vars:
            result.env_vars = {}
        assert isinstance(result.env_vars, dict), f"env_vars is not dict: {result.env_vars}"
        for key, val in result.env_vars.items():
            assert isinstance(key, str) and isinstance(val, str), f"invalid key:val {key}:{val}"
        if result.expected == "*":
            return result
        assert isinstance(result.expected, list), f"expected is not list: {result.expected}"
        for item in result.expected:
            assert isinstance(item, str), f"expected item is not str: {item}"
        return result


@dataclass
class Schema:
    commands: List[Cmd]

    @classmethod
    def from_json(cls, src: dict) -> 'Schema':
        commands = src['commands']
        assert isinstance(commands, list)
        return cls(commands=[Cmd.from_json(cmd) for cmd in commands])

