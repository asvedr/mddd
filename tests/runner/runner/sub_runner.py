import subprocess as sp
import json
import os
import os.path

from .schema import Schema, Cmd

class SubRunner:
    class BadSchema(Exception):
        pass

    class TestFail(Exception):
        pass

    def __init__(self, path):
        self.path = os.path.abspath(path)
        try:
            with open(f'{path}/schema.json') as h:
                js = json.load(h)
            self.schema = Schema.from_json(js)
        except Exception as err:
            raise self.BadSchema(str(err))

    def run(self):
        os.chdir(self.path)
        for cmd in self.schema.commands:
            try:
                self.run_cmd(cmd)
            except self.TestFail:
                raise
            except Exception as err:
                raise self.TestFail(str(err))

    def run_cmd(self, cmd: Cmd):
        print(f'{self.path}: running {cmd.cmd}') 
        for key, val in cmd.env_vars.items():
            os.environ[key] = val
        res = sp.check_output(cmd.cmd, shell=True)
        for key in cmd.env_vars.keys():
            os.environ[key] = ''
        lines = [
            line.strip()
            for line in res.decode().split('\n')
            if line.strip()
        ]
        if cmd.expected == "*":
            return
        if len(lines) != len(cmd.expected):
            raise self.TestFail(f'{lines} != {cmd.expected}')
        for line, exp in zip(lines, cmd.expected):
            if line != exp and exp != "*":
                raise self.TestFail(f'{repr(line)} != {repr(exp)}')

