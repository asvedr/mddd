import os
import sys
from .sub_runner import SubRunner

class MainRunner:
    def __init__(self, path: str):
        self.path = path

    def run(self):
        subdirs = self.collect()
        subdirs.sort()
        for subdir in subdirs:
            try:
                SubRunner(f'{self.path}/{subdir}').run()
            except SubRunner.BadSchema as err:
                print(f'{subdir} invalid schema: {err}')
                sys.exit(1)
            except SubRunner.TestFail as err:
                print(f'{subdir} failed: {err}')
                sys.exit(1)

    def collect(self):
        return [
            obj
            for obj in os.listdir(self.path)
            if obj.startswith('int-test')
        ]

