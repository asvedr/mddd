from os import path

from runner.main_runner import MainRunner

if __name__ == '__main__':
    main_dir = path.dirname(path.dirname(__file__))
    MainRunner(main_dir).run()

