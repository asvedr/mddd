use mddd::macros::auto_impl;

#[auto_impl(link, dyn)]
trait TForLinkAndDyn {
    fn name(&self) -> &str {"<none>"}
    fn as_str(&self) -> String;
}

#[auto_impl(link)]
trait TLinkOnly {
    type Out;
    fn zero() -> Self::Out;
    fn one() -> Self::Out;
    fn neg(&self) -> Self::Out;
    fn add(&self, other: &Self::Out) -> Self::Out;
}

#[auto_impl(link)]
trait TLinkGen<T> {
    fn to_gen(&self) -> T;
}

impl TLinkGen<u16> for u8 {
    fn to_gen(&self) -> u16 {*self as u16}
}

impl TLinkGen<u32> for u8 {
    fn to_gen(&self) -> u32 {*self as u32}
}

impl TLinkOnly for isize {
    type Out = isize;
    fn zero() -> isize {0}
    fn one() -> isize {1}
    fn neg(&self) -> isize {-self}
    fn add(&self, other: &isize) -> isize {self + other}
}

impl TForLinkAndDyn for usize {
    fn as_str(&self) -> String {
        self.to_string()
    }
}

impl TForLinkAndDyn for String {
    fn name(&self) -> &str {
        "string"
    }
    fn as_str(&self) -> String {
        self.clone()
    }
}

fn show_foo<F: TForLinkAndDyn>(f: F) {
    println!("{}:{}", f.name(), f.as_str())
}

fn recast<T: TLinkGen<u32>>(t: T) -> u32 {
    t.to_gen()
}

fn main() {
    show_foo(&2);
    show_foo(&&3);
    show_foo(&"x".to_string());
    let x: &dyn TForLinkAndDyn = &5;
    show_foo(x);
    let a = <isize as TLinkOnly>::one();
    let b = <&isize as TLinkOnly>::one();
    println!("{} == {}", a, b);
    println!("{} == {}", recast(8_u8), recast(&8_u8));
}

