use std::env;

use mddd_traits::IRunner;

pub(crate) fn run(mut runner: impl IRunner) {
    let args = env::args().collect::<Vec<_>>();
    let refs = args
        .iter()
        .skip(1)
        .map(|s| -> &str { s })
        .collect::<Vec<&str>>();
    runner.execute(&args[0], &refs)
}
