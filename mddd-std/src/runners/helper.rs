use mddd_traits::{IRunner, IUseCase, IUseCaseRequest};

pub(crate) fn make_leaf_help<R: IUseCaseRequest, UC: IUseCase>(prefix: &str, uc: &UC) -> String {
    format!("{}: {}\nArgs:\n{}", prefix, uc.description(), R::help())
}

pub(crate) fn make_node_help(
    prefix: &str,
    description: &str,
    children: &[(String, Box<dyn IRunner>)],
) -> String {
    let children_help = children
        .iter()
        .map(|(key, node)| format!("  {} - {}", key, node.short_description()))
        .collect::<Vec<_>>()
        .join("\n");
    format!("{}: {}\nCommands:\n{}", prefix, description, children_help)
}
