use std::mem;
use crate::constants::HELP_KEYS;
use crate::runners::helper::make_node_help;
use crate::runners::main_executor;
use crate::utils::print_n_exit;

use mddd_traits::IRunner;

pub struct NodeRunner {
    description: String,
    default_params: Vec<String>,
    runners: Vec<(String, Box<dyn IRunner>)>,
}

impl NodeRunner {
    pub fn new(description: impl ToString) -> Self {
        Self {
            description: description.to_string(),
            default_params: Default::default(),
            runners: Vec::new(),
        }
    }

    pub fn add(mut self, cmd: impl ToString, runner: impl IRunner + 'static) -> Self {
        self.runners.push((cmd.to_string(), Box::new(runner)));
        self
    }

    pub fn add_box(mut self, cmd: impl ToString, runner: Box<dyn IRunner>) -> Self {
        self.runners.push((cmd.to_string(), runner));
        self
    }

    pub fn set_default_params<T: ToString>(mut self, params: &[T]) -> Self {
        self.default_params = params.iter()
            .map(|p|p.to_string())
            .collect();
        self
    }

    pub fn run(self) {
        main_executor::run(self)
    }

    fn execute_command(&mut self, prefix: &str, cmd: &str, args: &[&str]) {
        let found = self.runners.iter_mut().find(|(key, _)| key == cmd);
        let child = match found {
            Some((_, val)) => val,
            None => print_n_exit(&format!("Invalid command: {}", cmd)),
        };
        let prefix = format!("{} {}", prefix, cmd);
        child.execute(&prefix, args)
    }

    fn execute_without_args(&mut self, prefix: &str) {
        if self.default_params.is_empty() {
            self.execute_with_args(prefix, &[HELP_KEYS[0]])
        } else {
            let args = mem::take(&mut self.default_params);
            let ptr_args = args.iter()
                .map(|s| -> &str {s})
                .collect::<Vec<_>>();
            self.execute_with_args(prefix, &ptr_args)
        }
    }

    fn execute_with_args(&mut self, prefix: &str, args: &[&str]) {
        if HELP_KEYS.contains(&args[0]) {
            let help = make_node_help(prefix, &self.short_description(), &self.runners);
            println!("{}", help);
        } else {
            self.execute_command(prefix, args[0], &args[1..])
        }
    }
}

impl IRunner for NodeRunner {
    fn short_description(&self) -> String {
        self.description.clone()
    }

    fn execute(&mut self, prefix: &str, args: &[&str]) {
        if args.is_empty() {
            self.execute_without_args(prefix)
        } else {
            self.execute_with_args(prefix, args)
        }
    }
}
