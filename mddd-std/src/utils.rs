use std::mem;

#[inline(always)]
pub fn to_static<T>(ptr: &T) -> &'static T {
    unsafe { mem::transmute(ptr) }
}

pub fn print_n_exit(msg: &str) -> ! {
    eprintln!("{}", msg);
    std::process::exit(1)
}
