use std::any::type_name;
use std::marker::PhantomData;
use std::str::FromStr;

use mddd_traits::IParser;

#[derive(Default)]
pub struct BoolParser;

#[derive(Default)]
pub struct FromStrParser<T> {
    phantom: PhantomData<T>,
}

#[derive(Default)]
pub struct StringParser;

#[derive(Default)]
pub struct OptParser<T> {
    phantom: PhantomData<T>,
}

impl IParser<bool> for BoolParser {
    fn parse(src: &str) -> Result<bool, String> {
        const VARIANTS: &[(&str, bool)] = &[
            ("1", true),
            ("y", true),
            ("t", true),
            ("0", false),
            ("n", false),
            ("f", false),
        ];
        let param = src.to_lowercase();
        for (var, val) in VARIANTS.iter() {
            if *var == param {
                return Ok(*val);
            }
        }
        Err("not a bool value".to_string())
    }
}

impl<T: FromStr + 'static> IParser<T> for FromStrParser<T> {
    fn parse(src: &str) -> Result<T, String> {
        if let Ok(val) = T::from_str(src) {
            return Ok(val);
        }
        let msg = format!("expected type {}", type_name::<T>());
        Err(msg)
    }
}

impl<T: 'static, P: IParser<T>> IParser<Option<T>> for OptParser<P> {
    fn parse(src: &str) -> Result<Option<T>, String> {
        Ok(Some(P::parse(src)?))
    }
}

impl IParser<String> for StringParser {
    fn parse(src: &str) -> Result<String, String> {
        Ok(src.to_string())
    }
}
