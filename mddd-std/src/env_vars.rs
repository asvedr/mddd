use std::collections::HashMap;
use std::env;
use std::mem;

pub struct EnvVars {
    #[allow(dead_code)]
    storage: Vec<(String, String)>,
    vars: HashMap<&'static str, &'static str>,
}

impl EnvVars {
    pub fn build() -> Self {
        let storage = env::vars()
            .filter(|(_, value)| !value.trim().is_empty())
            .map(|(key, value)| (key.to_uppercase(), value))
            .collect::<Vec<_>>();
        let vars = unsafe { Self::build_vars(&storage) };
        Self { storage, vars }
    }

    #[inline(always)]
    pub fn get_var(&self, name: &str) -> Option<&str> {
        let val = self.vars.get(&name)?;
        Some(*val)
    }

    unsafe fn build_vars(raw: &[(String, String)]) -> HashMap<&'static str, &'static str> {
        raw.iter()
            .map(|(key, val)| {
                let key_ref: &str = key;
                let key_ptr: &'static str = mem::transmute(key_ref);
                let val_ref: &str = val;
                let val_ptr: &'static str = mem::transmute(val_ref);
                (key_ptr, val_ptr)
            })
            .collect()
    }
}
