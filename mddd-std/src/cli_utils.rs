use crate::cli_params::CliParamsError;
use mddd_traits::IParser;

pub struct CliUtils {}

impl CliUtils {
    pub fn proc_cli_err_s<T, P: IParser<T>>(
        key: &str,
        res: Result<Option<&str>, CliParamsError>,
        default: Option<T>,
    ) -> Result<T, String> {
        match (res, default) {
            (Ok(Some(val)), _) => {
                P::parse(val).map_err(|msg| format!("Invalid field({}): {}", key, msg))
            }
            (Ok(None), Some(val)) => Ok(val),
            (Ok(None), None) => Err(format!("Field not set: {}", key)),
            (Err(err), _) => Err(format!("Invalid field({}): {:?}", key, err)),
        }
    }

    pub fn proc_cli_err_m<T, P: IParser<T>>(
        key: &str,
        res: Result<Vec<&str>, CliParamsError>,
    ) -> Result<Vec<T>, String> {
        let as_str = match res {
            Ok(val) => val,
            Err(err) => {
                let msg = format!("Invalid field({}): {:?}", key, err);
                return Err(msg);
            }
        };
        as_str
            .into_iter()
            .map(|src: &str| -> Result<T, String> { P::parse(src) })
            .collect::<Result<Vec<T>, String>>()
            .map_err(|msg| format!("Invalid field({}): {}", key, msg))
    }
}
