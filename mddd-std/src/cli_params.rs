#[derive(Debug)]
pub enum CliParamsError {
    TooManyValues,
    KeyRequiresValue,
}

pub struct CliParams<'a> {
    params: Vec<&'a str>,
}

impl<'a> CliParams<'a> {
    pub fn new(params: Vec<&'a str>) -> Self {
        Self { params }
    }

    pub fn pop_bool_flag(
        &mut self,
        short: Option<&str>,
        long: Option<&str>,
    ) -> Result<bool, CliParamsError> {
        let indexes = self.get_key_indexes(short, long);
        match indexes.len() {
            0 => Ok(false),
            1 => {
                self.params.remove(indexes[0]);
                Ok(true)
            }
            _ => Err(CliParamsError::TooManyValues),
        }
    }

    pub fn pop_key_value(
        &mut self,
        short: Option<&str>,
        long: Option<&str>,
    ) -> Result<Option<&'a str>, CliParamsError> {
        let indexes = self.get_key_indexes(short, long);
        match indexes.len() {
            0 => Ok(None),
            1 if indexes[0] == self.params.len() - 1 => Err(CliParamsError::KeyRequiresValue),
            1 => {
                let index = indexes[0];
                let val = self.params.remove(index + 1);
                self.params.remove(index);
                Ok(Some(val))
            }
            _ => Err(CliParamsError::TooManyValues),
        }
    }

    pub fn pop_key_values(
        &mut self,
        short: Option<&str>,
        long: Option<&str>,
    ) -> Result<Vec<&'a str>, CliParamsError> {
        let indexes = self.get_key_indexes(short, long);
        if indexes.is_empty() {
            return Ok(Vec::new());
        }
        if indexes.last().unwrap() + 1 == self.params.len() {
            return Err(CliParamsError::KeyRequiresValue);
        }
        let mut result = Vec::new();
        for index in indexes.into_iter().rev() {
            result.push(self.params.remove(index + 1));
            self.params.remove(index);
        }
        result.reverse();
        Ok(result)
    }

    pub fn pop_position_value(&mut self) -> Option<&'a str> {
        if self.params.is_empty() {
            return None;
        }
        Some(self.params.remove(0))
    }

    pub fn pop_position_values(&mut self) -> Vec<&'a str> {
        std::mem::take(&mut self.params)
    }

    fn get_key_indexes(&self, short: Option<&str>, long: Option<&str>) -> Vec<usize> {
        let mut result = Vec::new();
        for (i, key) in self.params.iter().enumerate() {
            if short == Some(key) || long == Some(key) {
                result.push(i);
            }
        }
        result
    }
}
