use crate::entities::constants::{AVAILABLE_CONTAINERS_FOR_MULTI_FIELD, META_FIELD};
use quote::ToTokens;

pub(crate) fn split_fields(fields: &syn::FieldsNamed) -> (Option<&syn::Field>, Vec<&syn::Field>) {
    let mut meta = None;
    let mut others = Vec::new();
    for field in fields.named.iter() {
        if field.ident.as_ref().unwrap() != META_FIELD {
            others.push(field);
            continue;
        }
        if field.ty.to_token_stream().to_string() != "()" {
            panic!("Meta field must be ()");
        }
        meta = Some(field);
    }
    (meta, others)
}

pub(crate) fn extract_ty_from_container(ty: &str) -> String {
    let split = ty.split(' ').collect::<Vec<_>>();
    if split.len() == 1 {
        panic!("mulivalue field must have contained type(vec or set)")
    }
    if !AVAILABLE_CONTAINERS_FOR_MULTI_FIELD.contains(&split[0]) {
        panic!("mulivalue field container is unsupported: {:?}", split[0]);
    }
    split[2..split.len() - 1].join(" ")
}
