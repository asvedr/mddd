use quote::ToTokens;
use std::collections::HashMap;

pub(crate) fn remove_brackets(text: &str) -> &str {
    let len = text.len();
    &text[1..len - 1]
}

pub(crate) fn get_attr(attrs: &[syn::Attribute], key: &str) -> Option<String> {
    for attr in attrs {
        let name = attr.path.segments.to_token_stream().to_string();
        if name != key {
            continue;
        }
        let tokens = attr.tokens.to_string();
        if tokens.len() < 2 {
            panic!("attr {} requires value", key)
        }
        return Some(remove_brackets(&tokens).to_string());
    }
    None
}

pub(crate) fn get_attrs(attrs: &[syn::Attribute], key: &str) -> Vec<String> {
    let mut result = Vec::new();
    for attr in attrs {
        let name = attr.path.segments.to_token_stream().to_string();
        if name != key {
            continue;
        }
        let tokens = attr.tokens.to_string();
        if tokens.len() < 2 {
            panic!("attr {} requires value", key)
        }
        result.push(remove_brackets(&tokens).to_string());
    }
    result
}

pub(crate) fn has_attr(attrs: &[syn::Attribute], key: &str) -> bool {
    for attr in attrs {
        let name = attr.path.segments.to_token_stream().to_string();
        if name == key {
            return true;
        }
    }
    false
}

pub(crate) fn process_text_attr<'a>(name: &str, val: &'a str) -> &'a str {
    if val.len() < 2 {
        panic!("attr {} must have string value", name)
    }
    val.trim().trim_start_matches('"').trim_end_matches('"')
}

pub(crate) fn remake_ty(mut ty: String, replacements: &HashMap<String, String>) -> String {
    for (from, to) in replacements {
        ty = ty.replace(from, to);
    }
    ty
}
