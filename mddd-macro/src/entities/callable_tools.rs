use crate::entities::constants::TIMESTAMP_PARAM;
use proc_macro::TokenStream;
use quote::ToTokens;
use std::str::FromStr;
use syn::FnArg;

pub struct AttrMacroParamMap {
    data: Vec<(String, String)>,
    place: String,
}

pub struct MacroFnArgs {
    pub args_to_declare: String,
    pub args_to_call_maker: String,
    pub cache_type: String,
    pub tuple_to_make_cache: String,
}

impl MacroFnArgs {
    pub fn with_ttl(src: Vec<&FnArg>) -> Self {
        let mut names = vec![TIMESTAMP_PARAM.to_string()];
        let mut tuple_types = vec!["u64".to_string()];
        for arg in src.iter() {
            let (name, tp) = split_fn_arg(arg);
            names.push(name);
            tuple_types.push(tp);
        }
        Self {
            args_to_declare: src
                .iter()
                .map(|a| a.to_token_stream().to_string())
                .collect::<Vec<_>>()
                .join(","),
            args_to_call_maker: names[1..].join(","),
            cache_type: format!("<({}) as CacheParam>::CacheType", tuple_types.join(",")),
            tuple_to_make_cache: format!(
                "({})",
                names
                    .iter()
                    .map(|a| format!("&{}", a))
                    .collect::<Vec<_>>()
                    .join(",")
            ),
        }
    }

    pub fn no_ttl(src: Vec<&FnArg>) -> Self {
        if src.len() == 1 {
            let (name, tp) = split_fn_arg(src[0]);
            return Self {
                args_to_declare: format!("{}: {}", name, tp),
                args_to_call_maker: name.clone(),
                cache_type: format!("<{} as CacheParam>::CacheType", tp),
                tuple_to_make_cache: name,
            };
        }
        let mut names = Vec::new();
        let mut tuple_types = Vec::new();
        for arg in src.iter() {
            let (name, tp) = split_fn_arg(arg);
            names.push(name);
            tuple_types.push(tp);
        }
        Self {
            args_to_declare: src
                .iter()
                .map(|a| a.to_token_stream().to_string())
                .collect::<Vec<_>>()
                .join(","),
            args_to_call_maker: names.join(","),
            cache_type: format!("<({}) as CacheParam>::CacheType", tuple_types.join(",")),
            tuple_to_make_cache: format!(
                "({})",
                names
                    .iter()
                    .map(|a| format!("&{}", a))
                    .collect::<Vec<_>>()
                    .join(",")
            ),
        }
    }
}

impl AttrMacroParamMap {
    pub fn new(place: String, src: TokenStream, valid_keys: &[&str]) -> Self {
        let text = src.to_string();
        let mut data = Vec::new();
        for item in text.split(',') {
            let (key, val) = match item.split_once('=') {
                None => (item.trim(), ""),
                Some((key, val)) => (key.trim(), val.trim()),
            };
            if key.is_empty() {
                continue;
            }
            if !valid_keys.contains(&key) {
                panic!("{}: got unexpected param: {:?}", place, key);
            }
            data.push((key.to_string(), val.to_string()))
        }
        Self { place, data }
    }

    pub fn has_key(&self, key: &str) -> bool {
        self.get_raw_key(key).is_some()
    }

    pub fn get_parsed_key<T: FromStr>(&self, key: &str) -> Option<T> {
        let raw = self.get_raw_key(key)?;
        match T::from_str(raw) {
            Ok(val) => Some(val),
            Err(_) => panic!("{} got invalid param: {}", self.place, key),
        }
    }

    pub fn get_raw_key(&self, expected: &str) -> Option<&str> {
        for (key, val) in self.data.iter() {
            if key == expected {
                return Some(val);
            }
        }
        None
    }
}

fn split_fn_arg(arg: &FnArg) -> (String, String) {
    match arg {
        FnArg::Receiver(_) => panic!("can not use 'self' in singleton or lru_cache"),
        FnArg::Typed(arg) => {
            let name = arg.pat.to_token_stream().to_string();
            let tp = arg.ty.to_token_stream().to_string();
            (name, tp)
        }
    }
}
