pub(crate) const META_FIELD: &str = "__meta__";
pub(crate) const PREFIX_ATTR: &str = "prefix";
pub(crate) const PARSER_ATTR: &str = "parser";
pub(crate) const DESCRIPTION_ATTR: &str = "description";
pub(crate) const NAME_ATTR: &str = "name";
pub(crate) const DEFAULT_ATTR: &str = "default";
pub(crate) const SHORT_ATTR: &str = "short";
pub(crate) const LONG_ATTR: &str = "long";
pub(crate) const MULTI_ATTR: &str = "multi";
pub(crate) const BOOL_FLAG_ATTR: &str = "bool_flag";

pub(crate) const STD_PARSER_STR: &str = "StringParser";
pub(crate) const STD_PARSER_BOOL: &str = "BoolParser";
pub(crate) const STD_PARSER_FROM_STR: &str = "FromStrParser";
pub(crate) const STD_PARSER_OPT: &str = "OptParser";
pub(crate) const OPT_PREFIX: &str = "Option <";
pub(crate) const AVAILABLE_CONTAINERS_FOR_MULTI_FIELD: &[&str] = &["Vec", "HashSet", "BTreeSet"];

pub(crate) const AUTO_FROM_ACCEPT: &str = "accept";
pub(crate) const AUTO_FROM_METHOD: &str = "method";
pub(crate) const AUTO_FROM_IGNORE: &str = "ignore_auto_from";

pub(crate) const CACHE_PARAM_MUTEX: &str = "mutex";
pub(crate) const CACHE_PARAM_NO_MUTEX: &str = "no_mutex";
pub(crate) const CACHE_PARAM_LEN: &str = "cache_len";
pub(crate) const CACHE_PARAM_TTL: &str = "ttl";
pub(crate) const SINGLETON_PARAM_TYPE: &str = "ty";

pub(crate) const TIMESTAMP_PARAM: &str = "ts";

pub(crate) const AUTO_IMPL_KEY_LINK: &str = "link";
pub(crate) const AUTO_IMPL_KEY_DYN: &str = "dyn";

pub(crate) const DERIVE_SETTERS_ASSIGN: &str = "assign";
pub(crate) const DERIVE_SETTERS_REMAKE: &str = "remake";
