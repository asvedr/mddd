use quote::ToTokens;
use std::collections::HashSet;
use syn::{Fields, FnArg, TraitItem, TraitItemMethod, Variant};

use crate::entities::constants::{
    AUTO_FROM_ACCEPT, AUTO_FROM_IGNORE, AUTO_FROM_METHOD, BOOL_FLAG_ATTR, DEFAULT_ATTR,
    DESCRIPTION_ATTR, LONG_ATTR, MULTI_ATTR, NAME_ATTR, OPT_PREFIX, PARSER_ATTR, PREFIX_ATTR,
    SHORT_ATTR, STD_PARSER_BOOL, STD_PARSER_FROM_STR, STD_PARSER_OPT, STD_PARSER_STR,
};
use crate::parsers::extract_ty_from_container;
use crate::utils::{get_attr, get_attrs, has_attr, process_text_attr};

#[derive(Default, Debug)]
pub(crate) struct ConfigMeta {
    pub(crate) prefix: String,
    pub(crate) has_meta_field: bool,
}

#[derive(Debug)]
pub(crate) struct ConfigFieldMeta {
    pub(crate) name: String,
    pub(crate) parser: String,
    pub(crate) ty: String,
    pub(crate) description: Option<String>,
    pub(crate) env_var: String,
    pub(crate) default: Option<String>,
}

#[derive(Debug)]
pub(crate) struct RequestFieldMeta {
    pub(crate) name: String,
    pub(crate) parser: String,
    pub(crate) ty: String,
    // field for contained(example: ty=Vec<usize>, inner_ty=usize)
    pub(crate) inner_ty: String,
    pub(crate) description: Option<String>,
    pub(crate) short: Option<String>,
    pub(crate) long: Option<String>,
    pub(crate) default: Option<String>,
    pub(crate) is_multiple: bool,
    pub(crate) is_bool_flag: bool,
}

#[derive(Debug)]
pub(crate) struct AutoDeriveVariantMeta {
    pub(crate) key: String,
    pub(crate) content_ty: Option<String>,
    pub(crate) accept: Vec<String>,
    pub(crate) custom_method: Option<String>,
    pub(crate) ignore: bool,
}

#[derive(Clone)]
pub(crate) enum AutoDeriveMethod {
    AsIs,
    Into,
    Lambda(String),
    Nothing,
}

pub(crate) struct AutoDeriveSchema {
    pub(crate) from_ty: String,
    pub(crate) var_id: String,
    pub(crate) method: AutoDeriveMethod,
}

pub(crate) struct TraitMethodMeta {
    pub(crate) decl_spec: String,
    pub(crate) call_name: String,
    pub(crate) call_args: Vec<String>,
    pub(crate) has_self: bool,
}

pub(crate) struct TraitMeta {
    pub(crate) consts: Vec<(String, String)>,
    pub(crate) methods: Vec<TraitMethodMeta>,
    pub(crate) types: Vec<String>,
}

impl TraitMethodMeta {
    fn parse(item: TraitItemMethod) -> Self {
        let decl_spec = Self::get_decl_spec(&item);
        let call_name = item.sig.ident.to_string();
        let mut call_args = vec![];
        let mut has_self = false;
        for arg in item.sig.inputs.into_iter() {
            match arg {
                FnArg::Receiver(_) => has_self = true,
                FnArg::Typed(arg) => call_args.push(arg.pat.to_token_stream().to_string()),
            }
        }
        Self {
            decl_spec,
            call_name,
            call_args,
            has_self,
        }
    }

    fn get_decl_spec(item: &TraitItemMethod) -> String {
        let text = item.to_token_stream().to_string();
        let (res, _) = if text.contains(';') {
            text.split_once(';').unwrap()
        } else {
            text.split_once('{').unwrap()
        };
        res.to_string()
    }
}

impl TraitMeta {
    pub(crate) fn parse(items: Vec<TraitItem>) -> Self {
        let mut consts = Vec::new();
        let mut methods = Vec::new();
        let mut types = Vec::new();
        for item in items {
            match item {
                TraitItem::Const(val) => {
                    let name = val.ident.to_string();
                    let ty = val.ty.to_token_stream().to_string();
                    consts.push((name, ty))
                }
                TraitItem::Type(val) => types.push(val.ident.to_string()),
                TraitItem::Method(val) => methods.push(TraitMethodMeta::parse(val)),
                other => panic!(
                    "unsupported trait item: {:?}",
                    other.to_token_stream().to_string()
                ),
            }
        }
        Self {
            consts,
            methods,
            types,
        }
    }
}

impl AutoDeriveVariantMeta {
    pub(crate) fn build(variant: &Variant) -> Self {
        let key = variant.ident.to_string();
        let accept = get_attrs(&variant.attrs, AUTO_FROM_ACCEPT);
        let content_ty = match variant.fields {
            Fields::Named(_) => panic!("EnumAutoFrom: named fields not supported"),
            Fields::Unnamed(ref val) => {
                if val.unnamed.len() != 1 {
                    panic!(
                        "EnumAutoFrom: variant content must have 0 or 1 fields, found: {}",
                        val.unnamed.len(),
                    )
                }
                let ty = val.unnamed.first().to_token_stream().to_string();
                Some(ty)
            }
            Fields::Unit => None,
        };
        let custom_method = get_attr(&variant.attrs, AUTO_FROM_METHOD);
        let ignore = has_attr(&variant.attrs, AUTO_FROM_IGNORE);
        Self {
            key,
            content_ty,
            accept,
            custom_method,
            ignore,
        }
    }
}

impl AutoDeriveMethod {
    fn remake(&self, from_ty: &str, to_ty: &str) -> Self {
        match self {
            AutoDeriveMethod::Into if from_ty == to_ty => AutoDeriveMethod::AsIs,
            AutoDeriveMethod::Into => AutoDeriveMethod::Into,
            _ => self.clone(),
        }
    }
}

impl AutoDeriveSchema {
    pub(crate) fn collect(variants: &[AutoDeriveVariantMeta]) -> Vec<Self> {
        let mut used = HashSet::new();
        let mut result = Vec::new();
        for variant in variants {
            for schema in Self::from_variant_meta(variant) {
                if used.contains(&schema.from_ty) {
                    panic!("can not impl From<{}> twice", schema.from_ty)
                }
                used.insert(schema.from_ty.clone());
                result.push(schema)
            }
        }
        result
    }

    fn from_variant_meta(meta: &AutoDeriveVariantMeta) -> Vec<Self> {
        if meta.ignore {
            return Vec::new();
        }
        let accept = if meta.accept.is_empty() && meta.content_ty.is_some() {
            vec![meta.content_ty.clone().unwrap()]
        } else {
            meta.accept.clone()
        };
        let (method, ty) = match (&meta.custom_method, &meta.content_ty) {
            (_, None) => (AutoDeriveMethod::Nothing, ""),
            (None, Some(ty)) => (AutoDeriveMethod::Into, ty as &str),
            (Some(func), Some(_)) => (AutoDeriveMethod::Lambda(func.clone()), ""),
        };
        accept
            .into_iter()
            .map(|from_ty| {
                let method = method.remake(&from_ty, ty);
                let var_id = meta.key.clone();
                Self {
                    from_ty,
                    var_id,
                    method,
                }
            })
            .collect()
    }
}

impl ConfigMeta {
    pub(crate) fn build(field: Option<&syn::Field>) -> Self {
        let field = match field {
            None => return Self::default(),
            Some(val) => val,
        };
        if let Some(prefix) = get_attr(&field.attrs, PREFIX_ATTR) {
            let prefix = process_text_attr(PREFIX_ATTR, &prefix);
            Self {
                prefix: prefix.to_uppercase(),
                has_meta_field: true,
            }
        } else {
            Self::default()
        }
    }
}

impl ConfigFieldMeta {
    pub(crate) fn build(meta: &ConfigMeta, field: &syn::Field) -> Self {
        let var_name = get_attr(&field.attrs, NAME_ATTR)
            .map(|val| process_text_attr(NAME_ATTR, &val).to_uppercase())
            .unwrap_or_else(|| field.ident.as_ref().unwrap().to_string().to_uppercase());
        let env_var = format!("{}{}", meta.prefix, var_name);
        let ty = field.ty.to_token_stream().to_string();
        let (mut parser, is_std_parser) = match get_attr(&field.attrs, PARSER_ATTR) {
            None => (make_std_ty_parser(&ty), true),
            Some(val) => (val, false),
        };
        let default = if ty.starts_with(OPT_PREFIX) {
            if !is_std_parser {
                parser = format!("{}<{}>", STD_PARSER_OPT, parser);
            }
            let default =
                get_attr(&field.attrs, DEFAULT_ATTR).unwrap_or_else(|| "None".to_string());
            Some(default)
        } else {
            get_attr(&field.attrs, DEFAULT_ATTR)
        };
        Self {
            parser,
            name: field.ident.as_ref().unwrap().to_string(),
            ty,
            description: get_attr(&field.attrs, DESCRIPTION_ATTR)
                .map(|val| process_text_attr(DESCRIPTION_ATTR, &val).to_string()),
            env_var,
            default,
        }
    }
}

impl RequestFieldMeta {
    pub(crate) fn build(field: &syn::Field) -> Self {
        let name = field.ident.as_ref().unwrap().to_string();
        let ty = field.ty.to_token_stream().to_string();
        let mut inner_ty = String::new();
        let is_multiple = has_attr(&field.attrs, MULTI_ATTR);
        let is_opt = !is_multiple && ty.starts_with(OPT_PREFIX);
        let (mut parser, is_std_parser) = match get_attr(&field.attrs, PARSER_ATTR) {
            None => (make_std_ty_parser(&ty), true),
            Some(val) => (val, false),
        };
        let mut default = None;
        match () {
            _ if is_opt && !is_std_parser => {
                parser = format!("{}<{}>", STD_PARSER_OPT, parser);
                default = Some(
                    get_attr(&field.attrs, DEFAULT_ATTR).unwrap_or_else(|| "None".to_string()),
                );
            }
            _ if is_opt => {
                default = Some(
                    get_attr(&field.attrs, DEFAULT_ATTR).unwrap_or_else(|| "None".to_string()),
                );
            }
            _ if is_multiple && is_std_parser => {
                inner_ty = extract_ty_from_container(&ty);
                parser = make_std_ty_parser(&inner_ty);
            }
            _ if is_multiple => {
                inner_ty = extract_ty_from_container(&ty);
            }
            _ => default = get_attr(&field.attrs, DEFAULT_ATTR),
        }
        Self {
            name,
            parser,
            ty,
            inner_ty,
            description: get_attr(&field.attrs, DESCRIPTION_ATTR)
                .map(|val| process_text_attr(DESCRIPTION_ATTR, &val).to_string()),
            short: get_attr(&field.attrs, SHORT_ATTR)
                .map(|val| process_text_attr(SHORT_ATTR, &val).to_string()),
            long: get_attr(&field.attrs, LONG_ATTR)
                .map(|val| process_text_attr(LONG_ATTR, &val).to_string()),
            default,
            is_multiple,
            is_bool_flag: has_attr(&field.attrs, BOOL_FLAG_ATTR),
        }
    }
}

fn make_std_ty_parser(ty: &str) -> String {
    if ty.starts_with(OPT_PREFIX) {
        let ty_len = ty.len();
        let inner_ty = &ty[OPT_PREFIX.len()..ty_len - 1];
        format!("{}<{}>", STD_PARSER_OPT, make_std_ty_parser_inner(inner_ty))
    } else {
        make_std_ty_parser_inner(ty)
    }
}

fn make_std_ty_parser_inner(ty: &str) -> String {
    match ty {
        "String" => STD_PARSER_STR.to_string(),
        "bool" => STD_PARSER_BOOL.to_string(),
        _ => format!("{}<{}>", STD_PARSER_FROM_STR, ty),
    }
}
