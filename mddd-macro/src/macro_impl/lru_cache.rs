use proc_macro::TokenStream;
use quote::ToTokens;
use syn::{parse_macro_input, ItemFn, Visibility};

use crate::entities::callable_tools::{AttrMacroParamMap, MacroFnArgs};
use crate::entities::constants::{
    CACHE_PARAM_LEN, CACHE_PARAM_MUTEX, CACHE_PARAM_NO_MUTEX, CACHE_PARAM_TTL, TIMESTAMP_PARAM,
};

const GET_NOW: &str = "SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs()";

pub fn lru_cache(attr: TokenStream, input: TokenStream) -> TokenStream {
    let parsed = parse_macro_input!(input as ItemFn);
    let vis = match parsed.vis {
        Visibility::Public(_) => "pub ".to_string(),
        Visibility::Crate(_) => "pub(crate) ".to_string(),
        Visibility::Restricted(val) => {
            let path = val.path.to_token_stream().to_string();
            format!("pub({}) ", path)
        }
        Visibility::Inherited => "".to_string(),
    };
    let params = parsed.sig.inputs.iter().collect::<Vec<_>>();
    let name = parsed.sig.ident.to_string();
    let attrs: LruCacheAttrs = LruCacheAttrs::parse(&name, attr);
    let ty = parsed.sig.output.to_token_stream().to_string();
    let body = parsed.block.to_token_stream().to_string();
    if ty.is_empty() {
        panic!("cached function {:?} has unit type", name);
    }
    let ty = &ty[3..];
    let len = attrs.cache_len;
    let code: String = match (params.is_empty(), attrs.mutex_mode, attrs.ttl) {
        (true, true, None) => build_mutex_no_params_no_ttl(&vis, &name, ty, &body),
        (true, true, Some(ttl)) => build_mutex_no_params_ttl(&vis, &name, ty, &body, ttl),
        (true, false, None) => build_no_mutex_no_params_no_ttl(&vis, &name, ty, &body),
        (true, false, Some(ttl)) => build_no_mutex_no_params_ttl(&vis, &name, ty, &body, ttl),
        (false, true, None) => {
            build_mutex_params_no_ttl(&vis, &name, ty, &body, MacroFnArgs::no_ttl(params), len)
        }
        (false, true, Some(ttl)) => build_mutex_params_ttl(
            &vis,
            &name,
            ty,
            &body,
            MacroFnArgs::with_ttl(params),
            len,
            ttl,
        ),
        (false, false, None) => {
            build_no_mutex_params_no_ttl(&vis, &name, ty, &body, MacroFnArgs::no_ttl(params), len)
        }
        (false, false, Some(ttl)) => build_no_mutex_params_ttl(
            &vis,
            &name,
            ty,
            &body,
            MacroFnArgs::with_ttl(params),
            len,
            ttl,
        ),
    };
    code.parse().unwrap()
}

struct LruCacheAttrs {
    mutex_mode: bool,
    cache_len: usize,
    ttl: Option<u64>,
}

impl LruCacheAttrs {
    fn parse(func: &str, src: TokenStream) -> Self {
        let place = format!("mddd::macros::lru_cache({})", func);
        let map = AttrMacroParamMap::new(
            place.clone(),
            src,
            &[
                CACHE_PARAM_MUTEX,
                CACHE_PARAM_NO_MUTEX,
                CACHE_PARAM_LEN,
                CACHE_PARAM_TTL,
            ],
        );
        if map.has_key(CACHE_PARAM_MUTEX) && map.has_key(CACHE_PARAM_NO_MUTEX) {
            panic!(
                "{}: Can not use {} and {} in same time",
                place, CACHE_PARAM_MUTEX, CACHE_PARAM_NO_MUTEX,
            )
        }
        let mutex_mode = map.has_key(CACHE_PARAM_MUTEX) || !map.has_key(CACHE_PARAM_NO_MUTEX);
        let cache_len = map.get_parsed_key(CACHE_PARAM_LEN).unwrap_or(100);
        let ttl = map.get_parsed_key(CACHE_PARAM_TTL);
        Self {
            mutex_mode,
            cache_len,
            ttl,
        }
    }
}

fn build_no_mutex_no_params_no_ttl(vis: &str, name: &str, ty: &str, body: &str) -> String {
    format!(
        r#"
            {vis}fn {name}() -> {ty} {op}
                fn maker() -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Option<{ty}> = None;
                unsafe {op}
                    if let Some(ref value) = STORAGE {op}
                        return value.clone()
                    {cl}
                    STORAGE = Some(maker());
                    if let Some(ref value) = STORAGE {op}
                        return value.clone()
                    {cl}
                {cl}
                unreachable!()
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        body = body,
        op = '{',
        cl = '}'
    )
}

fn build_no_mutex_no_params_ttl(vis: &str, name: &str, ty: &str, body: &str, ttl: u64) -> String {
    format!(
        r#"
            {vis}fn {name}() -> {ty} {op}
                use std::time::SystemTime;

                fn maker() -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Option<{ty}> = None;
                static mut TIME: u64 = 0;
                let now = {get_now};
                unsafe {op}
                    if now - TIME > {ttl} {op} STORAGE = None {cl}
                    if let Some(ref value) = STORAGE {op}
                        return value.clone()
                    {cl}
                    STORAGE = Some(maker());
                    if let Some(ref value) = STORAGE {op}
                        return value.clone()
                    {cl}
                {cl}
                unreachable!()
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        body = body,
        get_now = GET_NOW,
        ttl = ttl,
        op = '{',
        cl = '}'
    )
}

fn build_mutex_no_params_no_ttl(vis: &str, name: &str, ty: &str, body: &str) -> String {
    format!(
        r#"
            {vis}fn {name}() -> {ty} {op}
                use std::sync::Mutex;

                fn maker() -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Mutex<Option<{ty}>> = Mutex::new(None);
                let mut guard = unsafe {op} STORAGE.lock().unwrap() {cl};
                if let Some(ref value) = *guard {op}
                    return value.clone()
                {cl}
                *guard = Some(maker());
                if let Some(ref value) = *guard {op}
                    return value.clone()
                {cl}
                unreachable!()
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        body = body,
        op = '{',
        cl = '}'
    )
}

fn build_mutex_no_params_ttl(vis: &str, name: &str, ty: &str, body: &str, ttl: u64) -> String {
    format!(
        r#"
            {vis}fn {name}() -> {ty} {op}
                use std::sync::Mutex;
                use std::time::SystemTime;

                fn maker() -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Mutex<Option<(u64, {ty})>> = Mutex::new(None);
                let mut guard = unsafe {op} STORAGE.lock().unwrap() {cl};
                let now = {get_now};
                if let Some((ref last_time, ref value)) = *guard {op}
                    if now - last_time <= {ttl} {op}
                        return value.clone()
                    {cl}
                {cl}
                let new_value = maker();
                *guard = Some((now, new_value.clone()));
                new_value
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        body = body,
        get_now = GET_NOW,
        ttl = ttl,
        op = '{',
        cl = '}'
    )
}

fn build_mutex_params_no_ttl(
    vis: &str,
    name: &str,
    ty: &str,
    body: &str,
    args: MacroFnArgs,
    max_len: usize,
) -> String {
    format!(
        r#"
            {vis}fn {name}({args_dec}) -> {ty} {op}
                use std::sync::Mutex;
                use std::collections::VecDeque;
                use mddd::traits::CacheParam;

                fn maker({args_dec}) -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Mutex<VecDeque<({args_cache}, {ty})>> = Mutex::new(VecDeque::new());
                let mut guard = unsafe {op} STORAGE.lock().unwrap() {cl};
                let cache_key = {args_tpl}.to_cache_type();
                for (key, val) in guard.iter() {op}
                    if *key == cache_key {op}
                        return val.clone()
                    {cl}
                {cl}
                let new_val = maker({args_call});
                guard.push_back((cache_key, new_val.clone()));
                if guard.len() > {max_len} {op}
                    guard.pop_front();
                {cl}
                new_val
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        max_len = max_len,
        body = body,
        args_dec = args.args_to_declare,
        args_cache = args.cache_type,
        args_tpl = args.tuple_to_make_cache,
        args_call = args.args_to_call_maker,
        op = '{',
        cl = '}'
    )
}

fn build_mutex_params_ttl(
    vis: &str,
    name: &str,
    ty: &str,
    body: &str,
    args: MacroFnArgs,
    max_len: usize,
    ttl: u64,
) -> String {
    format!(
        r#"
            {vis}fn {name}({args_dec}) -> {ty} {op}
                use std::sync::Mutex;
                use std::time::SystemTime;
                use std::collections::VecDeque;
                use mddd::traits::CacheParam;

                fn maker({args_dec}) -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Mutex<VecDeque<({args_cache}, {ty})>> = Mutex::new(VecDeque::new());
                let {ts_var} = {get_now} / {ttl};
                let mut guard = unsafe {op} STORAGE.lock().unwrap() {cl};
                let cache_key = {args_tpl}.to_cache_type();
                for (key, val) in guard.iter() {op}
                    if *key == cache_key {op}
                        return val.clone()
                    {cl}
                {cl}
                let new_val = maker({args_call});
                guard.push_back((cache_key, new_val.clone()));
                if guard.len() > {max_len} {op}
                    guard.pop_front();
                {cl}
                new_val
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        max_len = max_len,
        body = body,
        args_dec = args.args_to_declare,
        args_cache = args.cache_type,
        args_tpl = args.tuple_to_make_cache,
        args_call = args.args_to_call_maker,
        ts_var = TIMESTAMP_PARAM,
        get_now = GET_NOW,
        ttl = ttl,
        op = '{',
        cl = '}'
    )
}

fn build_no_mutex_params_no_ttl(
    vis: &str,
    name: &str,
    ty: &str,
    body: &str,
    args: MacroFnArgs,
    max_len: usize,
) -> String {
    format!(
        r#"
            {vis}fn {name}({args_dec}) -> {ty} {op}
                use std::collections::VecDeque;
                use mddd::traits::CacheParam;

                fn maker({args_dec}) -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: VecDeque<({cache_type},{ty})> = VecDeque::new();
                let cache_key = {args_tpl}.to_cache_type();
                unsafe {op}
                    for (key, val) in STORAGE.iter() {op}
                        if *key == cache_key {op}
                            return val.clone()
                        {cl}
                    {cl}
                    let new_val = maker({args_call});
                    STORAGE.push_back((cache_key, new_val.clone()));
                    if STORAGE.len() > {max_len} {op}
                        STORAGE.pop_front();
                    {cl}
                    new_val
                {cl}
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        max_len = max_len,
        body = body,
        args_dec = args.args_to_declare,
        cache_type = args.cache_type,
        args_tpl = args.tuple_to_make_cache,
        args_call = args.args_to_call_maker,
        op = '{',
        cl = '}'
    )
}

fn build_no_mutex_params_ttl(
    vis: &str,
    name: &str,
    ty: &str,
    body: &str,
    args: MacroFnArgs,
    max_len: usize,
    ttl: u64,
) -> String {
    format!(
        r#"
            {vis}fn {name}({args_dec}) -> {ty} {op}
                use std::collections::VecDeque;
                use std::time::SystemTime;
                use mddd::traits::CacheParam;

                fn maker({args_dec}) -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: VecDeque<({cache_type},{ty})> = VecDeque::new();
                let {ts_var} = {get_now} / {ttl};
                let cache_key = {args_tpl}.to_cache_type();
                unsafe {op}
                    for (key, val) in STORAGE.iter() {op}
                        if *key == cache_key {op}
                            return val.clone()
                        {cl}
                    {cl}
                    let new_val = maker({args_call});
                    STORAGE.push_back((cache_key, new_val.clone()));
                    if STORAGE.len() > {max_len} {op}
                        STORAGE.pop_front();
                    {cl}
                    new_val
                {cl}
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        max_len = max_len,
        body = body,
        args_dec = args.args_to_declare,
        cache_type = args.cache_type,
        args_tpl = args.tuple_to_make_cache,
        args_call = args.args_to_call_maker,
        ts_var = TIMESTAMP_PARAM,
        get_now = GET_NOW,
        ttl = ttl,
        op = '{',
        cl = '}'
    )
}
