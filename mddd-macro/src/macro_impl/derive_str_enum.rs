use crate::utils::{get_attr, process_text_attr};
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput, Variant};

pub(crate) fn derive_str_enum(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as DeriveInput);
    let name = input.ident.to_string();
    let variants = match input.data {
        syn::Data::Enum(ref val) => val.variants.iter().map(parse_variant).collect::<Vec<_>>(),
        _ => panic!("Сan not derive not struct"),
    };
    let str_to_val = make_str_to_val(&name, &variants);
    let val_to_str = make_val_to_str(&name, &variants);
    let variants_code = make_variants(&name, &variants);
    let code = format!(
        r#"
            impl mddd::traits::IStrEnum for {name} {op}
                fn variants() -> &'static [Self] {op}
                    {variants_code}
                {cl}
                fn val_to_str(&self) -> &'static str {op}
                    {val_to_str}
                {cl}
                fn str_to_val(src: &str) -> Result<Self, mddd::traits::InvalidEnumValue<String>> {op}
                    {str_to_val}
                {cl}
            {cl}
        "#,
        op = '{',
        cl = '}',
        name = name,
        val_to_str = val_to_str,
        str_to_val = str_to_val,
        variants_code = variants_code,
    );
    code.parse().unwrap()
}

fn parse_variant(var: &Variant) -> (String, String) {
    let key = var.ident.to_string();
    if let Some(val) = get_attr(&var.attrs, "str_value") {
        (key, process_text_attr("str_value", &val).to_string())
    } else {
        (key.clone(), key)
    }
}

fn make_val_to_str(name: &str, variants: &[(String, String)]) -> String {
    let cases = variants
        .iter()
        .map(|(key, val)| format!("{}::{} => \"{}\"", name, key, val))
        .collect::<Vec<_>>()
        .join(",");
    format!(
        r#"
        match self {op}
            {cases}
        {cl}"#,
        op = '{',
        cl = '}',
        cases = cases,
    )
}

fn make_str_to_val(name: &str, variants: &[(String, String)]) -> String {
    let mut cases = variants
        .iter()
        .map(|(key, val)| format!("\"{}\" => Ok({}::{})", val, name, key))
        .collect::<Vec<_>>();
    let dflt = format!(
        r#"_ => Err(mddd::traits::InvalidEnumValue {op}
            typename: "{name}", value: src.to_string(),
        {cl})"#,
        op = '{',
        cl = '}',
        name = name
    );
    cases.push(dflt);
    format!(
        r#"
        match src {op}
            {cases}
        {cl}"#,
        op = '{',
        cl = '}',
        cases = cases.join(","),
    )
}

fn make_variants(name: &str, variants: &[(String, String)]) -> String {
    let variants = variants
        .iter()
        .map(|(key, _)| format!("{}::{}", name, key))
        .collect::<Vec<_>>()
        .join(",");
    format!(
        r#"
            const VALUE: &'static [{name}] = &[{variants}];
            VALUE
        "#,
        name = name,
        variants = variants,
    )
}
