use crate::entities::callable_tools::{AttrMacroParamMap, MacroFnArgs};
use crate::entities::constants::{CACHE_PARAM_MUTEX, CACHE_PARAM_NO_MUTEX, SINGLETON_PARAM_TYPE};
use proc_macro::TokenStream;
use quote::ToTokens;
use syn::{parse_macro_input, ItemFn, Visibility};

pub fn singleton(attr: TokenStream, input: TokenStream) -> TokenStream {
    let parsed = parse_macro_input!(input as ItemFn);
    let vis = match parsed.vis {
        Visibility::Public(_) => "pub ".to_string(),
        Visibility::Crate(_) => "pub(crate) ".to_string(),
        Visibility::Restricted(val) => {
            let path = val.path.to_token_stream().to_string();
            format!("pub({}) ", path)
        }
        Visibility::Inherited => "".to_string(),
    };
    let params = parsed.sig.inputs.iter().collect::<Vec<_>>();
    let name = parsed.sig.ident.to_string();
    let macro_attrs = SingletonAttrs::parse(&name, attr);
    let ty = parsed.sig.output.to_token_stream().to_string();
    let body = parsed.block.to_token_stream().to_string();
    if ty.is_empty() {
        panic!("singleton {:?} has unit type", name);
    }
    let ty = &ty[3..];
    let gty = macro_attrs
        .force_type
        .unwrap_or_else(|| format!("&'static {}", ty));
    let code = match (params.is_empty(), macro_attrs.mutex_mode) {
        (true, true) => build_mutex_no_params(&vis, &name, ty, &gty, &body),
        (true, false) => build_no_mutex_no_params(&vis, &name, ty, &gty, &body),
        (false, true) => {
            build_mutex_params(&vis, &name, ty, &gty, &body, MacroFnArgs::no_ttl(params))
        }
        (false, false) => {
            build_no_mutex_params(&vis, &name, ty, &gty, &body, MacroFnArgs::no_ttl(params))
        }
    };
    code.parse().unwrap()
}

struct SingletonAttrs {
    mutex_mode: bool,
    force_type: Option<String>,
}

impl SingletonAttrs {
    fn parse(func: &str, src: TokenStream) -> Self {
        let place = format!("mddd::macros::singleton({})", func);
        let map = AttrMacroParamMap::new(
            place.clone(),
            src,
            &[
                CACHE_PARAM_MUTEX,
                CACHE_PARAM_NO_MUTEX,
                SINGLETON_PARAM_TYPE,
            ],
        );
        if map.has_key(CACHE_PARAM_MUTEX) && map.has_key(CACHE_PARAM_NO_MUTEX) {
            panic!(
                "{}: Can not use {} and {} in same time",
                place, CACHE_PARAM_MUTEX, CACHE_PARAM_NO_MUTEX,
            )
        }
        let mutex_mode = map.has_key(CACHE_PARAM_MUTEX) || !map.has_key(CACHE_PARAM_NO_MUTEX);
        let force_type = map.get_raw_key(SINGLETON_PARAM_TYPE).map(|s| s.to_string());
        Self {
            mutex_mode,
            force_type,
        }
    }
}

fn build_no_mutex_no_params(vis: &str, name: &str, ty: &str, gty: &str, body: &str) -> String {
    format!(
        r#"
            {vis}fn {name}() -> {gty} {op}
                fn maker() -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Option<{ty}> = None;
                unsafe {op}
                    if let Some(ref value) = STORAGE {op}
                        return value
                    {cl}
                    STORAGE = Some(maker());
                    if let Some(ref value) = STORAGE {op}
                        return value
                    {cl}
                {cl}
                unreachable!()
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        gty = gty,
        body = body,
        op = '{',
        cl = '}'
    )
}

fn build_mutex_no_params(vis: &str, name: &str, ty: &str, gty: &str, body: &str) -> String {
    format!(
        r#"
            {vis}fn {name}() -> {gty} {op}
                use std::sync::Mutex;
                fn maker() -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Mutex<Option<{ty}>> = Mutex::new(None);
                let mut guard = unsafe {op} STORAGE.lock().unwrap() {cl};
                if let Some(ref value) = *guard {op}
                    let ptr: &'static {ty} = mddd::std::utils::to_static(value);
                    return ptr as {gty}
                {cl}
                *guard = Some(maker());
                if let Some(ref value) = *guard {op}
                    let ptr: &'static {ty} = mddd::std::utils::to_static(value);
                    return ptr as {gty}
                {cl}
                unreachable!()
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        gty = gty,
        body = body,
        op = '{',
        cl = '}'
    )
}

fn build_mutex_params(
    vis: &str,
    name: &str,
    ty: &str,
    gty: &str,
    body: &str,
    args: MacroFnArgs,
) -> String {
    format!(
        r#"
            {vis}fn {name}({args_dec}) -> {gty} {op}
                use std::sync::Mutex;
                use mddd::traits::CacheParam;

                fn maker({args_dec}) -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Mutex<Vec<({args_cache}, {ty})>> = Mutex::new(Vec::new());
                let mut guard = unsafe {op} STORAGE.lock().unwrap() {cl};
                let cache_key = {args_tpl}.to_cache_type();
                for (key, val) in guard.iter() {op}
                    if *key == cache_key {op}
                        let ptr: &'static {ty} = mddd::std::utils::to_static(val);
                        return ptr as {gty}
                    {cl}
                {cl}
                guard.push((cache_key, maker({args_call})));
                let (_, val) = guard.last().unwrap();
                let ptr: &'static {ty} = mddd::std::utils::to_static(val);
                ptr as {gty}
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        gty = gty,
        body = body,
        args_dec = args.args_to_declare,
        args_cache = args.cache_type,
        args_tpl = args.tuple_to_make_cache,
        args_call = args.args_to_call_maker,
        op = '{',
        cl = '}'
    )
}

fn build_no_mutex_params(
    vis: &str,
    name: &str,
    ty: &str,
    gty: &str,
    body: &str,
    args: MacroFnArgs,
) -> String {
    format!(
        r#"
            {vis}fn {name}({args_dec}) -> {gty} {op}
                use mddd::traits::CacheParam;

                fn maker({args_dec}) -> {ty} {op}
                    {body}
                {cl}
                static mut STORAGE: Vec<({cache_type},{ty})> = Vec::new();
                let cache_key = {args_tpl}.to_cache_type();
                unsafe {op}
                    for (key, val) in STORAGE.iter() {op}
                        if *key == cache_key {op}
                            let ptr: &'static {ty} = mddd::std::utils::to_static(val);
                            return ptr as {gty}
                        {cl}
                    {cl}
                    STORAGE.push((cache_key, maker({args_call})));
                    let (_, val) = STORAGE.last().unwrap();
                    let ptr: &'static {ty} = mddd::std::utils::to_static(val);
                    ptr as {gty}
                {cl}
            {cl}
        "#,
        vis = vis,
        name = name,
        ty = ty,
        gty = gty,
        body = body,
        args_dec = args.args_to_declare,
        cache_type = args.cache_type,
        args_tpl = args.tuple_to_make_cache,
        args_call = args.args_to_call_maker,
        op = '{',
        cl = '}'
    )
}
