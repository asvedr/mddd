use crate::entities::constants::META_FIELD;
use crate::entities::meta::{ConfigFieldMeta, ConfigMeta};
use crate::parsers::split_fields;
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput, Fields};

pub(crate) fn derive_function(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as DeriveInput);
    let fields = match input.data {
        syn::Data::Struct(ref val) => &val.fields,
        _ => panic!("Сan not derive not struct"),
    };
    let fields = match fields {
        Fields::Named(ref val) => val,
        _ => panic!("Deriving is allowed only for named fields"),
    };
    let (m_total, m_fields) = collect_meta(fields);
    let build = make_build_fn(&m_total, &m_fields);
    let help = make_help_fn(&m_fields);
    let code = format!(
        "impl mddd::traits::IConfig for {name} {op} {build} {help} {cl}",
        op = '{',
        cl = '}',
        name = input.ident,
        build = build,
        help = help
    );
    code.parse().unwrap()
}

fn collect_meta(fields: &syn::FieldsNamed) -> (ConfigMeta, Vec<ConfigFieldMeta>) {
    let (meta, common_fields) = split_fields(fields);
    let meta = ConfigMeta::build(meta);
    let mut result_fields = Vec::new();
    for field in common_fields {
        result_fields.push(ConfigFieldMeta::build(&meta, field))
    }
    (meta, result_fields)
}

fn make_build_fn(meta: &ConfigMeta, fields: &[ConfigFieldMeta]) -> String {
    let mut constructors = if meta.has_meta_field {
        format!("{}: (),", META_FIELD)
    } else {
        String::new()
    };
    for field in fields {
        let constructor = if field.default.is_some() {
            make_field_constructor_default(field)
        } else {
            make_field_constructor(field)
        };
        constructors.push_str(&constructor);
    }
    format!(
        r#"
            fn build() -> Result<Self, String> {op}
                use mddd::traits::IParser;
                use mddd::std::parsers::{op}BoolParser, FromStrParser, OptParser, StringParser{cl};
                use mddd::std::env_vars::EnvVars;

                let env_vars = EnvVars::build();

                fn parse<T, P: IParser<T>>(val: &str) -> Result<T, String> {op}
                    P::parse(val)
                {cl}

                let config = Self {op}
                    {fields}
                {cl};
                Ok(config)
            {cl}
        "#,
        op = '{',
        cl = '}',
        fields = constructors
    )
}

fn make_field_constructor_default(meta: &ConfigFieldMeta) -> String {
    let default = meta.default.as_ref().unwrap();
    format!(
        r#"
            {name}: match env_vars.get_var("{env}") {op}
                Some(val) => parse::<{ty}, {parser}>(val)?,
                _ => {default},
            {cl},
        "#,
        name = meta.name,
        env = meta.env_var,
        ty = meta.ty,
        parser = meta.parser,
        default = default,
        op = '{',
        cl = '}'
    )
}

fn make_field_constructor(meta: &ConfigFieldMeta) -> String {
    format!(
        r#"
            {name}: match env_vars.get_var("{env}") {op}
                Some(val) => parse::<{ty}, {parser}>(val)?,
                None => return Err("var {env} not set".to_string()),
            {cl},
        "#,
        name = meta.name,
        env = meta.env_var,
        ty = meta.ty,
        parser = meta.parser,
        op = '{',
        cl = '}'
    )
}

fn make_help_fn(fields: &[ConfigFieldMeta]) -> String {
    let mut fields_code = String::new();
    for field in fields {
        let dflt = match field.default {
            None => "".to_string(),
            Some(ref val) => format!(" (default={})", val),
        };
        let descr = match field.description {
            None => "".to_string(),
            Some(ref val) => format!(", {}", val),
        };
        let code = format!(
            r#"result.push_str(r#"{name}: {ty}{dflt}{descr}{nl}"{hash});"#,
            name = field.env_var,
            ty = field.ty,
            dflt = dflt,
            descr = descr,
            nl = "\n",
            hash = "#"
        );
        fields_code.push_str(&code);
    }
    format!(
        r#"
            fn help() -> String {op}
                let mut result = String::new();
                {fields}
                result
            {cl}
        "#,
        fields = fields_code,
        op = '{',
        cl = '}'
    )
}
