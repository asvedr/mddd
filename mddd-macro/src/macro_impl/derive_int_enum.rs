use crate::utils::get_attr;
use proc_macro::TokenStream;
use std::str::FromStr;
use syn::{parse_macro_input, DeriveInput, Variant};

pub(crate) fn derive_int_enum(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as DeriveInput);
    let name = input.ident.to_string();
    let variants = match input.data {
        syn::Data::Enum(ref val) => val
            .variants
            .iter()
            .enumerate()
            .map(parse_variant)
            .collect::<Vec<_>>(),
        _ => panic!("Сan not derive not struct"),
    };
    let int_to_val = make_int_to_val(&name, &variants);
    let val_to_int = make_val_to_int(&name, &variants);
    let variants_code = make_variants(&name, &variants);
    let code = format!(
        r#"
            impl mddd::traits::IIntEnum for {name} {op}
                fn variants() -> &'static [Self] {op}
                    {variants_code}
                {cl}
                fn val_to_int(&self) -> isize {op}
                    {val_to_int}
                {cl}
                fn int_to_val(src: isize) -> Result<Self, mddd::traits::InvalidEnumValue<isize>> {op}
                    {int_to_val}
                {cl}
            {cl}
        "#,
        op = '{',
        cl = '}',
        name = name,
        val_to_int = val_to_int,
        int_to_val = int_to_val,
        variants_code = variants_code,
    );
    code.parse().unwrap()
}

fn parse_variant(var: (usize, &Variant)) -> (String, isize) {
    let (index, var) = var;
    let key = var.ident.to_string();
    if let Some(val) = get_attr(&var.attrs, "int_value") {
        let val = match isize::from_str(&val.replace(' ', "")) {
            Ok(val) => val,
            Err(_) => panic!("int_val attr expected int value"),
        };
        (key, val)
    } else {
        (key, index as isize)
    }
}

fn make_val_to_int(name: &str, variants: &[(String, isize)]) -> String {
    let cases = variants
        .iter()
        .map(|(key, val)| format!("{}::{} => {}", name, key, val))
        .collect::<Vec<_>>()
        .join(",");
    format!(
        r#"
        match self {op}
            {cases}
        {cl}"#,
        op = '{',
        cl = '}',
        cases = cases,
    )
}

fn make_int_to_val(name: &str, variants: &[(String, isize)]) -> String {
    let mut cases = variants
        .iter()
        .map(|(key, val)| format!("{} => Ok({}::{})", val, name, key))
        .collect::<Vec<_>>();
    let dflt = format!(
        r#"_ => Err(mddd::traits::InvalidEnumValue {op}
            typename: "{name}", value: src,
        {cl})"#,
        op = '{',
        cl = '}',
        name = name
    );
    cases.push(dflt);
    format!(
        r#"
        match src {op}
            {cases}
        {cl}"#,
        op = '{',
        cl = '}',
        cases = cases.join(","),
    )
}

fn make_variants(name: &str, variants: &[(String, isize)]) -> String {
    let variants = variants
        .iter()
        .map(|(key, _)| format!("{}::{}", name, key))
        .collect::<Vec<_>>()
        .join(",");
    format!(
        r#"
            const VALUE: &'static [{name}] = &[{variants}];
            VALUE
        "#,
        name = name,
        variants = variants,
    )
}
