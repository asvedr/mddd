use proc_macro::TokenStream;
use quote::ToTokens;
use syn::{parse_macro_input, ItemTrait};

use crate::entities::callable_tools::AttrMacroParamMap;
use crate::entities::constants::{AUTO_IMPL_KEY_DYN, AUTO_IMPL_KEY_LINK};
use crate::entities::meta::{TraitMeta, TraitMethodMeta};

pub(crate) fn auto_impl(attr: TokenStream, item: TokenStream) -> TokenStream {
    let original_code = item.to_string();
    let parsed = parse_macro_input!(item as ItemTrait);
    let name = parsed.ident.to_string();
    let generics = parsed.generics.to_token_stream().to_string();
    let meta = TraitMeta::parse(parsed.items);
    let attr_map = AttrMacroParamMap::new(format!("trait {}", name), attr, &["link", "dyn"]);
    let mut code = original_code;
    let has_link = attr_map.has_key(AUTO_IMPL_KEY_LINK);
    let has_dyn = attr_map.has_key(AUTO_IMPL_KEY_DYN);
    if !(has_link || has_dyn) {
        panic!(
            "auto_impl must contain at least one key of [{}, {}]",
            AUTO_IMPL_KEY_LINK, AUTO_IMPL_KEY_DYN,
        )
    }
    if has_link {
        code.push_str("\n\n");
        code.push_str(&impl_link(&name, &generics, &meta));
    }
    if has_dyn {
        code.push_str("\n\n");
        code.push_str(&impl_dyn(&name, &generics, &meta));
    }
    code.parse().unwrap()
}

const TEMPLATE: &str = "__T";

fn impl_link(name: &str, generics: &str, meta: &TraitMeta) -> String {
    let pure_generics = generics
        .trim()
        .trim_start_matches('<')
        .trim_end_matches('>');
    let gen_prefix = if pure_generics.is_empty() {
        "".to_string()
    } else {
        format!("{},", pure_generics)
    };
    let mut attrs = Vec::new();
    for (c_name, ty) in meta.consts.iter() {
        attrs.push(format!(
            "const {c_name}: {ty} = {templ}::{c_name};",
            c_name = c_name,
            ty = ty,
            templ = TEMPLATE,
        ))
    }
    for t_name in meta.types.iter() {
        attrs.push(format!(
            "type {t_name} = {templ}::{t_name};",
            t_name = t_name,
            templ = TEMPLATE,
        ))
    }
    for method in meta.methods.iter() {
        attrs.push(make_method_link(method))
    }
    format!(
        r#"
            impl<{gen_prefix}{templ}: {name}{generics}> {name}{generics} for &{templ} {op}
                {attrs}
            {cl}
        "#,
        op = '{',
        cl = '}',
        name = name,
        generics = generics,
        gen_prefix = gen_prefix,
        templ = TEMPLATE,
        attrs = attrs.join("\n")
    )
}

fn impl_dyn(name: &str, generics: &str, meta: &TraitMeta) -> String {
    if !generics.is_empty() {
        panic!("can not impl dyn for {} because trait has generics", name);
    }
    if !meta.consts.is_empty() {
        panic!(
            "can not impl dyn for {} because trait contains consts",
            name
        );
    }
    if !meta.types.is_empty() {
        panic!("can not impl dyn for {} because trait contains types", name);
    }
    if !meta.methods.iter().all(|m| m.has_self) {
        panic!(
            "can not impl dyn for {} because trait contains method without self",
            name
        );
    }
    format!(
        r#"
            impl {name} for &dyn {name} {op}
                {methods}
            {cl}
        "#,
        op = '{',
        cl = '}',
        name = name,
        methods = meta
            .methods
            .iter()
            .map(make_method_dyn)
            .collect::<Vec<_>>()
            .join("\n")
    )
}

fn make_method_link(method: &TraitMethodMeta) -> String {
    let call_args = if method.has_self {
        format!("self,{}", method.call_args.join(","))
    } else {
        method.call_args.join(",")
    };
    format!(
        "{decl}{op} {ty}::{m_name}({args}) {cl}",
        op = '{',
        cl = '}',
        decl = method.decl_spec,
        ty = TEMPLATE,
        m_name = method.call_name,
        args = call_args,
    )
}

fn make_method_dyn(method: &TraitMethodMeta) -> String {
    format!(
        "{decl}{op}(**self).{m_name}({args}){cl}",
        op = '{',
        cl = '}',
        decl = method.decl_spec,
        m_name = method.call_name,
        args = method.call_args.join(",")
    )
}
