use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};

use crate::entities::meta::RequestFieldMeta;

pub(crate) fn derive_function(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as DeriveInput);
    let fields = match input.data {
        syn::Data::Struct(ref val) => &val.fields,
        _ => panic!("Сan not derive not struct"),
    };
    let m_fields = fields
        .into_iter()
        .map(RequestFieldMeta::build)
        .collect::<Vec<_>>();
    let build = make_build_fn(&m_fields);
    let help = make_help_fn(&m_fields);
    let code = format!(
        r#"
            impl mddd::traits::IUseCaseRequest for {name}{op}
                {build}
                {help}
            {cl}
        "#,
        name = input.ident,
        build = build,
        help = help,
        op = '{',
        cl = '}',
    );
    code.parse().unwrap()
}

fn make_build_fn(m_fields: &[RequestFieldMeta]) -> String {
    let mut actions = String::new();
    let mut constructors = String::new();
    for field in sort_fields(m_fields) {
        let action = make_constructor(field);
        actions.push_str(&action);
        let constructor = format!("{},", field.name);
        constructors.push_str(&constructor);
    }
    format!(
        r#"
            fn build(src: &[&str]) -> Result<Self, String> {op}
                use mddd::std::parsers::{op}BoolParser, FromStrParser, StringParser, OptParser{cl};
                use mddd::std::cli_params::{op}CliParams, CliParamsError{cl};
                use mddd::std::cli_utils::CliUtils;

                let mut params = CliParams::new(src.to_vec());
                {actions}
                let self_ = Self {op}
                    {constructors}
                {cl};
                Ok(self_)
            {cl}
        "#,
        actions = actions,
        constructors = constructors,
        op = '{',
        cl = '}',
    )
}

fn make_constructor(meta: &RequestFieldMeta) -> String {
    let has_key = meta.short.is_some() || meta.long.is_some();
    match () {
        _ if meta.is_bool_flag && has_key => bool_flag_constructor(meta),
        _ if meta.is_bool_flag => panic!("bool flag must have key"),
        _ if has_key && meta.is_multiple => multi_key_constructor(meta),
        _ if has_key => single_key_constructor(meta),
        _ if meta.is_multiple => multi_position_constructor(meta),
        _ => single_position_constructor(meta),
    }
}

fn bool_flag_constructor(meta: &RequestFieldMeta) -> String {
    format!(
        r#"
            let short: Option<&str> = {short:?};
            let long: Option<&str> = {long:?};
            let {name} = params.pop_bool_flag(short, long)
                .map_err(|err|
                    format!("Invalid field {name}: {op}:?{cl}", err)
                )?;
        "#,
        name = meta.name,
        short = meta.short,
        long = meta.long,
        op = '{',
        cl = '}'
    )
}

fn single_key_constructor(meta: &RequestFieldMeta) -> String {
    let dflt = match meta.default {
        Some(ref val) => format!("Some({})", val),
        None => "None".to_string(),
    };
    format!(
        r#"
            let short: Option<&str> = {short:?};
            let long: Option<&str> = {long:?};
            let {name} = CliUtils::proc_cli_err_s::<{ty},{parser}>(
                "{name}",
                params.pop_key_value(short, long),
                {dflt},
            )?;
        "#,
        short = meta.short,
        long = meta.long,
        name = meta.name,
        ty = meta.ty,
        parser = meta.parser,
        dflt = dflt,
    )
}

fn multi_key_constructor(meta: &RequestFieldMeta) -> String {
    let convert = if meta.ty.starts_with("Vec") {
        ""
    } else {
        ".into_iter().collect()"
    };
    format!(
        r#"
            let short: Option<&str> = {short:?};
            let long: Option<&str> = {long:?};
            let {name}: {var_ty} = CliUtils::proc_cli_err_m::<{ty},{parser}>(
                "{name}",
                params.pop_key_values(short, long),
            )?{convert};
        "#,
        short = meta.short,
        long = meta.long,
        name = meta.name,
        var_ty = meta.ty,
        ty = meta.inner_ty,
        parser = meta.parser,
        convert = convert
    )
}

fn single_position_constructor(meta: &RequestFieldMeta) -> String {
    let dflt = match meta.default {
        Some(ref val) => format!("Some({})", val),
        None => "None".to_string(),
    };
    format!(
        r#"
            let {name} = CliUtils::proc_cli_err_s::<{ty},{parser}>(
                "{name}",
                Ok(params.pop_position_value()),
                {dflt},
            )?;
        "#,
        name = meta.name,
        ty = meta.ty,
        parser = meta.parser,
        dflt = dflt,
    )
}

fn multi_position_constructor(meta: &RequestFieldMeta) -> String {
    let convert = if meta.ty.starts_with("Vec") {
        ""
    } else {
        ".into_iter().collect()"
    };
    format!(
        r#"
            let {name}: {var_ty} = CliUtils::proc_cli_err_m::<{ty},{parser}>(
                "{name}",
                Ok(params.pop_position_values()),
            )?{convert};
        "#,
        name = meta.name,
        var_ty = meta.ty,
        ty = meta.inner_ty,
        parser = meta.parser,
        convert = convert
    )
}

// First key params
// Second position params
fn sort_fields(fields: &[RequestFieldMeta]) -> Vec<&RequestFieldMeta> {
    let mut with_keys = Vec::new();
    let mut without_keys = Vec::new();
    for field in fields {
        if field.short.is_some() || field.long.is_some() {
            with_keys.push(field);
        } else {
            without_keys.push(field);
        }
    }
    with_keys.append(&mut without_keys);
    with_keys
}

fn make_help_fn(m_fields: &[RequestFieldMeta]) -> String {
    let mut actions = String::new();
    for field in m_fields {
        let action = match () {
            _ if field.is_multiple => make_multi_help(field),
            _ if field.is_bool_flag => make_flag_help(field),
            _ => make_field_help(field),
        };
        actions.push_str(&action);
    }
    format!(
        r#"
            fn help() -> String {op}
                let mut result = String::new();
                {actions}
                result
            {cl}
        "#,
        actions = actions,
        op = '{',
        cl = '}'
    )
}

fn make_field_help(field: &RequestFieldMeta) -> String {
    let short = if let Some(ref val) = field.short {
        format!("{} ", val)
    } else {
        "".to_string()
    };
    let long = if let Some(ref val) = field.long {
        format!("{} ", val)
    } else {
        "".to_string()
    };
    let dflt = if let Some(ref val) = field.default {
        format!(", (default={})", val)
    } else {
        "".to_string()
    };
    let description = if let Some(ref val) = field.description {
        format!(", {}", val)
    } else {
        "".to_string()
    };
    format!(
        "result.push_str(r#\"  {sh}{lo}{name}: {ty}{dflt}{description}\n\"#);",
        name = field.name,
        sh = short,
        lo = long,
        ty = field.ty,
        dflt = dflt,
        description = description
    )
}

fn make_flag_help(field: &RequestFieldMeta) -> String {
    let short = if let Some(ref val) = field.short {
        format!("{} ", val)
    } else {
        "".to_string()
    };
    let long = if let Some(ref val) = field.long {
        format!("{} ", val)
    } else {
        "".to_string()
    };
    let description = if let Some(ref val) = field.description {
        format!(", {}", val)
    } else {
        "".to_string()
    };
    format!(
        "result.push_str(\"  {sh}{lo}{name}: bool flag{description}\\n\");",
        name = field.name,
        sh = short,
        lo = long,
        description = description
    )
}

fn make_multi_help(field: &RequestFieldMeta) -> String {
    let short = if let Some(ref val) = field.short {
        format!("{} ", val)
    } else {
        "".to_string()
    };
    let long = if let Some(ref val) = field.long {
        format!("{} ", val)
    } else {
        "".to_string()
    };
    let description = if let Some(ref val) = field.description {
        format!(", {}", val)
    } else {
        "".to_string()
    };
    format!(
        "result.push_str(\"  {sh}{lo}many[{name}]: {ty}{description}\\n\");",
        name = field.name,
        sh = short,
        lo = long,
        ty = field.inner_ty,
        description = description
    )
}
