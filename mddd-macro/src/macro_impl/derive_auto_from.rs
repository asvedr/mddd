use proc_macro::TokenStream;
use quote::ToTokens;
use syn::{parse_macro_input, DeriveInput};

use crate::entities::meta::{AutoDeriveMethod, AutoDeriveSchema, AutoDeriveVariantMeta};

pub fn derive_auto_from(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as DeriveInput);
    let name = input.ident.to_string();
    let generics = input.generics.to_token_stream().to_string();
    let variants = match input.data {
        syn::Data::Enum(ref val) => val
            .variants
            .iter()
            .map(AutoDeriveVariantMeta::build)
            .collect::<Vec<_>>(),
        _ => panic!("Сan not derive not enum"),
    };
    let code = AutoDeriveSchema::collect(&variants)
        .into_iter()
        .map(|schema| gen_impl_code(&name, &generics, schema))
        .collect::<Vec<_>>()
        .join("\n");
    code.parse().unwrap()
}

fn gen_impl_code(enum_name: &str, generics: &str, derive_schema: AutoDeriveSchema) -> String {
    let var = "__value";
    let fun_body = match derive_schema.method {
        AutoDeriveMethod::Nothing => format!("{}::{}", enum_name, derive_schema.var_id),
        AutoDeriveMethod::AsIs => format!("{}::{}({})", enum_name, derive_schema.var_id, var),
        AutoDeriveMethod::Into => {
            format!("{}::{}({}.into())", enum_name, derive_schema.var_id, var)
        }
        AutoDeriveMethod::Lambda(fun) => {
            format!(
                r#"
                    let __m = {fun};
                    {enum_name}::{variant}(__m({var}))
                "#,
                fun = fun,
                enum_name = enum_name,
                variant = derive_schema.var_id,
                var = var,
            )
        }
    };
    format!(
        r#"
            impl{generics} From<{from_ty}> for {enum_name}{generics} {op}
                fn from({var}: {from_ty}) -> Self {op}
                    {body}
                {cl}
            {cl}
        "#,
        generics = generics,
        from_ty = derive_schema.from_ty,
        enum_name = enum_name,
        var = var,
        body = fun_body,
        op = '{',
        cl = '}',
    )
}
