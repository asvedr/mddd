use proc_macro::TokenStream;
use quote::ToTokens;
use std::collections::HashMap;
use syn::{parse_macro_input, Data, DeriveInput, Field};

use crate::entities::constants::{DERIVE_SETTERS_ASSIGN, DERIVE_SETTERS_REMAKE};
use crate::utils::{get_attr, has_attr, remake_ty};

const SETTER_PREFIX: &str = "set_";

pub fn derive_setters(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as DeriveInput);
    let name = input.ident.to_string();
    let generics = input
        .generics
        .params
        .iter()
        .map(|t| t.to_token_stream().to_string())
        .collect::<Vec<_>>();
    // let generics = input.generics.to_token_stream().to_string();
    let fields = match input.data {
        Data::Struct(val) => val.fields,
        _ => panic!("StructSetters can not be derived to {}: not a struct", name),
    };
    let mut setters = Vec::new();
    let field_names = fields
        .iter()
        .map(|f| f.ident.as_ref().unwrap().to_string())
        .collect::<Vec<_>>();
    for field in fields {
        let setter_code = if has_attr(&field.attrs, DERIVE_SETTERS_ASSIGN) {
            make_setter_assign(field)
        } else if has_attr(&field.attrs, DERIVE_SETTERS_REMAKE) {
            make_setter_remake(&name, &generics, &field_names, field)
        } else {
            continue;
        };
        setters.push(setter_code);
    }
    if setters.is_empty() {
        return "".parse().unwrap();
    }
    let code = format!(
        "impl{generics} {name}{generics}{op}{methods}{cl}",
        op = '{',
        cl = '}',
        generics = input.generics.to_token_stream().to_string(),
        name = name,
        methods = setters.join("\n")
    );
    code.parse().unwrap()
}

fn make_setter_assign(field: Field) -> String {
    format!(
        r#"
            pub fn {prefix}{f_name}(mut self, val: {ty}) -> Self {op}
                self.{f_name} = val;
                self
            {cl}
        "#,
        op = '{',
        cl = '}',
        prefix = SETTER_PREFIX,
        f_name = field.ident.as_ref().unwrap().to_string(),
        ty = field.ty.to_token_stream(),
    )
}

fn make_setter_remake(
    str_name: &str,
    generics: &[String],
    field_names: &[String],
    field: Field,
) -> String {
    let att_val = get_attr(&field.attrs, DERIVE_SETTERS_REMAKE).unwrap();
    let generics_to_replace = att_val
        .split(',')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect::<Vec<_>>();
    if generics_to_replace.is_empty() {
        return make_setter_assign(field);
    }
    let replace_map = make_new_generics(&generics_to_replace);
    let new_f_type = remake_ty(field.ty.to_token_stream().to_string(), &replace_map);
    let new_s_type = remake_ty(
        format!("{} < {} >", str_name, generics.join(" , ")),
        &replace_map,
    );
    let f_name = field.ident.as_ref().unwrap().to_string();
    let old_fields = field_names
        .iter()
        .filter(|name| *name != &f_name)
        .map(|name| format!("{name}: self.{name}", name = name))
        .collect::<Vec<_>>()
        .join(", ");
    let method_gen = replace_map.values().cloned().collect::<Vec<_>>().join(",");
    format!(
        r#"
            pub fn {prefix}{f_name}<{method_gen}>(self, {f_name}: {new_f_type}) -> {new_s_type}{op}
                {s_name} {op}
                    {old_fields}, {f_name}
                {cl}
            {cl}
        "#,
        op = '{',
        cl = '}',
        prefix = SETTER_PREFIX,
        f_name = f_name,
        s_name = str_name,
        method_gen = method_gen,
        new_f_type = new_f_type,
        new_s_type = new_s_type,
        old_fields = old_fields,
    )
}

fn make_new_generics(to_replace: &[&str]) -> HashMap<String, String> {
    let mut result = HashMap::new();
    for gen in to_replace {
        let name = if gen.starts_with('\'') {
            format!("'__lt_{}", result.len())
        } else {
            format!("__TP_{}", result.len())
        };
        result.insert(gen.to_string(), name);
    }
    result
}
