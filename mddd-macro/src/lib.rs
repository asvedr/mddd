extern crate proc_macro;

mod entities;
mod macro_impl;
mod parsers;
mod utils;

use proc_macro::TokenStream;

#[proc_macro_derive(IConfig, attributes(parser, prefix, description, name, default))]
pub fn derive_config_macro(item: TokenStream) -> TokenStream {
    macro_impl::derive_config::derive_function(item)
}

#[proc_macro_derive(
    IUseCaseRequest,
    attributes(parser, description, default, short, long, multi, bool_flag)
)]
pub fn derive_use_case_request(item: TokenStream) -> TokenStream {
    macro_impl::derive_use_case_request::derive_function(item)
}

#[proc_macro_derive(IStrEnum, attributes(str_value))]
pub fn derive_str_enum(item: TokenStream) -> TokenStream {
    macro_impl::derive_str_enum::derive_str_enum(item)
}

#[proc_macro_derive(IIntEnum, attributes(int_value))]
pub fn derive_int_enum(item: TokenStream) -> TokenStream {
    macro_impl::derive_int_enum::derive_int_enum(item)
}

#[proc_macro_attribute]
pub fn singleton(attr: TokenStream, item: TokenStream) -> TokenStream {
    macro_impl::singleton::singleton(attr, item)
}

#[proc_macro_attribute]
pub fn lru_cache(attr: TokenStream, item: TokenStream) -> TokenStream {
    macro_impl::lru_cache::lru_cache(attr, item)
}

#[proc_macro_derive(EnumAutoFrom, attributes(accept, method, ignore_auto_from))]
pub fn derive_auto_from(item: TokenStream) -> TokenStream {
    macro_impl::derive_auto_from::derive_auto_from(item)
}

#[proc_macro_attribute]
pub fn auto_impl(attr: TokenStream, item: TokenStream) -> TokenStream {
    macro_impl::auto_impl::auto_impl(attr, item)
}

#[proc_macro_derive(StructSetters, attributes(assign, remake))]
pub fn derive_setters(item: TokenStream) -> TokenStream {
    macro_impl::derive_setters::derive_setters(item)
}
